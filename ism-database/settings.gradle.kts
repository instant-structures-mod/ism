rootProject.name = "ism-database"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
    }
}
