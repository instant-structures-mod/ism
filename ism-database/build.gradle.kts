import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.21"
    id("org.jetbrains.dokka") version "1.6.21"
    id("com.github.ben-manes.versions") version "0.42.0"
    id("com.dorongold.task-tree") version "2.1.0"
    id("io.gitlab.arturbosch.detekt") version "1.20.0"
    kotlin("plugin.serialization") version "1.6.21"

    jacoco
    `java-library`
}

val archivesBaseName = "database"
group = "de.ism.$archivesBaseName"
version = "1.0-SNAPSHOT"

sourceSets {
    test {
        java {
            srcDirs("$buildDir/generated/src/test/java/")
        }
    }
}

repositories {
    mavenCentral()
}

val ktLinter: Configuration by configurations.creating

dependencies {
    ktLinter("com.pinterest:ktlint:0.45.2")
    api(kotlin("stdlib", "1.6.21"))

    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.3")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.8.2")
    testImplementation("org.assertj:assertj-core:3.22.0")
    testImplementation("javax.annotation:javax.annotation-api:1.3.2")
    testImplementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.3.0")
}

/**
 * Flag if Gradle runs from CI
 */
var ciMode: Boolean = false

tasks.named("dependencyUpdates", DependencyUpdatesTask::class.java).configure {
    checkForGradleUpdate = true
    outputFormatter = "txt"
    outputDir = "build/reports/dependencyUpdates/"
    reportfileName = "report"
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "1.8"
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"

tasks.test {
    if (project.hasProperty("ci")) {
        ciMode = true
    }

    useJUnitPlatform()

    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    if (project.hasProperty("ci")) {
        ciMode = true
    }
    dependsOn(tasks.test)
}

tasks.jacocoTestReport {
    reports {
        reports.xml.required.set(true)
        reports.csv.required.set(true)
        reports.html.required.set(true)
    }
}

jacoco {
    toolVersion = "0.8.7"
}

configurations.all {
    resolutionStrategy {
        eachDependency {
            if ("org.jacoco" == requested.group) {
                useVersion("0.8.7")
            }
        }
    }
}

detekt {
    buildUponDefaultConfig = true

    config = files("$projectDir/config/detekt.yml")
    baseline = file("$projectDir/config/baseline.xml")
}

tasks.register<Copy>("unpackFiles") {
    val path = "$buildDir/libs/${project.name}/"
    delete(path)

    from(zipTree(tasks.jar.get().archiveFile.get()))

    into(path)
}

tasks {
    withType<io.gitlab.arturbosch.detekt.Detekt> {
        jvmTarget = "1.8"

        if (project.hasProperty("ci")) {
            ciMode = true
        }
    }
}

fun isNonStable(version: String): Boolean {
    val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
    val regex = "^[0-9,.v-]+(-r)?$".toRegex()
    val isStable = stableKeyword || regex.matches(version)
    return isStable.not()
}

tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf {
        isNonStable(candidate.version) && !isNonStable(currentVersion)
    }
}

tasks.withType<Test> {
    systemProperties["junit.jupiter.execution.parallel.enabled"] = true
    systemProperties["junit.jupiter.execution.parallel.mode.default"] = "concurrent"
    maxParallelForks = (Runtime.getRuntime().availableProcessors() / 2).takeIf { it > 0 } ?: 1
}

val outputDir = "${project.buildDir}/reports/ktlint/"
val inputFiles = project.fileTree(mapOf("dir" to "src", "include" to "**/*.kt"))

val ktLinterCheck by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    description = "Check Kotlin code style."
    classpath = ktLinter
    mainClass.set("com.pinterest.ktlint.Main")
    args = listOf(
        "--reporter=plain",
        "--reporter=checkstyle,output=$buildDir/reports/ktlint.xml",
        "src/**/*.kt",
        "*.kts"
    )
}

val ktLinterFormat by tasks.creating(JavaExec::class) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    description = "Fix Kotlin code style deviations."
    classpath = ktLinter
    mainClass.set("com.pinterest.ktlint.Main")
    args = listOf("-F", "src/**/*.kt", "*.kts")
}

tasks.jar {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    from(
        configurations.compileClasspath.get().map {
            if (it.isDirectory) it else zipTree(it)
        }
    )
}
