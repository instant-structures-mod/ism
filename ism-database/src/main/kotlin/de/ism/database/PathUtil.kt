package de.ism.database

import java.nio.file.Path
import java.nio.file.Paths

/**
 * Created on 2021.03.14 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
object PathUtil {

    val assetsPath: Path = Paths.get(PathUtil::class.java.getResource("/assets/")!!.toURI())
}
