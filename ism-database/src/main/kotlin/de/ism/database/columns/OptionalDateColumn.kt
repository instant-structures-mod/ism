package de.ism.database.columns

import java.time.LocalDate

/**
 * Holds optional dates. Absent dates are marked by two zero-bytes.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class OptionalDateColumn(
    private val table: ByteArray,
) : AbstractList<LocalDate?>(), IColumn<LocalDate?> {

    override val size: Int
        get() = table.size / 2

    override fun get(index: Int): LocalDate? {
        val first = table[2 * index + 0].toInt()
        val second = table[2 * index + 1].toInt()

        return if (first == 0 && second == 0) {
            null
        } else {
            DateColumn.convert(first, second)
        }
    }
}
