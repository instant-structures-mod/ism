package de.ism.database.columns

/**
 * Holds values that a converted from two bytes.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class SixteenBitColumn<E>(
    private val table: ByteArray,
) : AbstractList<E>(), IColumn<E> {

    override val size: Int
        get() = table.size / 2

    override fun get(index: Int): E {
        return convert(table[2 * index], table[2 * index + 1])
    }

    abstract fun convert(first: Byte, second: Byte): E
}
