package de.ism.database.columns

import de.ism.database.base.IUri
import de.ism.database.base.Uri
import de.ism.database.huffman.CodeTree
import de.ism.database.tables.init.CapacityFactor

/**
 * Holds the source a project.
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class ProjectSourceColumn(
    table: Array<ByteArray>,
    codeTree: CodeTree,
    capacityFactor: CapacityFactor,
) : HuffmanColumn<IUri>(table, codeTree, capacityFactor) {

    override fun convert(array: ByteArray): IUri {
        return Uri(PREFIX + array.decodeToString())
    }

    companion object {
        const val PREFIX: String = "https://www.planetminecraft.com/project/"
    }
}
