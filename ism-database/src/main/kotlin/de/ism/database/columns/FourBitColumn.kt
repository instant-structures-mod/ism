package de.ism.database.columns

/**
 * Stores 16 different numerical values.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class FourBitColumn(
    /**
     * Maps 16 different indices to a numerical value.
     */
    private val map: IntArray,
    /**
     * Holds the indices map's indices.
     */
    private val table: ByteArray,
) : AbstractList<Int>(), IColumn<Int> {

    init {
        require(map.size == 16) { "size is ${map.size} instead of 16" }
    }

    override val size: Int
        get() = 2 * table.size

    override fun get(index: Int): Int {
        return map[getMapIndex(index)]
    }

    private fun getMapIndex(index: Int): Int {
        val tableIndex = index / 2
        val isLower = index % 2 == 0
        val value = table[tableIndex].toInt()

        return if (isLower) {
            value.shr(4) and 0x0F
        } else {
            value and 0x0F
        }
    }
}
