package de.ism.database.columns

import de.ism.database.huffman.BitInputStream
import de.ism.database.huffman.CodeTree
import de.ism.database.huffman.HuffmanDecompress
import de.ism.database.tables.init.CapacityFactor
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.math.roundToInt

/**
 * Stores the written-out variant of a tag.
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class FullTextColumn(
    private val table: Array<ByteArray>,
    private val markerCode: Int,
    private val codeTree: CodeTree,
    private val capacityFactor: CapacityFactor,
) : AbstractList<String>(), IColumn<String> {

    override val size: Int
        get() = table.size

    @Suppress("DuplicatedCode")
    override fun get(index: Int): String {
        val byteArray = table[index]
        val inputStream = ByteArrayInputStream(byteArray)
        val bitStream = BitInputStream(inputStream)
        val capacity = (byteArray.size * capacityFactor).roundToInt()
        val outputStream = ByteArrayOutputStream(capacity)

        var hasReadMarker = false
        while (true) {
            val symbol = HuffmanDecompress.read(codeTree, bitStream)
            // EOF symbol
            if (symbol == 256 || symbol == ';'.code) {
                break
            } else if (symbol == markerCode) {
                if (hasReadMarker) {
                    outputStream.reset()
                } else {
                    hasReadMarker = true
                }
            } else {
                outputStream.write(symbol)
            }
        }

        val out = outputStream.toByteArray()
        return out.decodeToString()
    }
}
