package de.ism.database.columns

import de.ism.database.base.IImage
import de.ism.database.base.Image
import de.ism.database.huffman.BitInputStream
import de.ism.database.huffman.CodeTree
import de.ism.database.huffman.HuffmanDecompress
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.math.roundToInt

/**
 *
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class ImagesColumn(
    table: Array<ByteArray>,
    codeTree: CodeTree,
    capacityFactor: Float,
) : HuffmanColumn<Set<IImage>>(table, codeTree, capacityFactor) {

    override fun convert(array: ByteArray): Set<IImage> {
        if (array.isEmpty()) {
            return emptySet()
        }

        var startIndex = 1
        for (index in startIndex..array.lastIndex) {
            val cur = array[index]
            if (cur == COMPRESSION_MARKER) {
                startIndex = index + 1
                break
            }
        }

        /*
         * compression string of this row
         */
        val compressed = array.sliceArray(1..startIndex - 2)

        val prefix = prefixToText(array[0])
        val set = mutableSetOf<IImage>()
        var sb = StringBuilder(prefix)

        fun handle(byte: Byte): StringBuilder {
            sb.append(suffixToText(byte))

            val image = Image(sb.toString())
            set.add(image)

            return StringBuilder(prefix)
        }

        fun handleMarker() {
            for (char in compressed) {
                if (char in SUFFIX_ONE_MARKER..SUFFIX_THREE_MARKER) {
                    sb = handle(char)
                } else {
                    sb.append(char.toInt().toChar())
                }
            }
        }

        for (index in startIndex..array.lastIndex) {

            when (val cur = array[index]) {
                COMPRESSION_MARKER -> handleMarker()
                in SUFFIX_ONE_MARKER..SUFFIX_THREE_MARKER -> sb = handle(cur)
                else -> sb.append(cur.toInt().toChar())
            }
        }

        return set
    }

    /**
     * This decode-function contradicts the layering principle to read a bit instead of a bit first.
     */
    override fun decode(byteArray: ByteArray): ByteArray {
        val inputStream = ByteArrayInputStream(byteArray)
        val bitStream = BitInputStream(inputStream)
        val capacity = (byteArray.size * capacityFactor).roundToInt()
        val outputStream = ByteArrayOutputStream(capacity)

        outputStream.write(bitStream.read())

        while (true) {
            val symbol = HuffmanDecompress.read(codeTree, bitStream)
            // EOF symbol
            if (symbol == 256 || symbol == ';'.code) {
                break
            }
            outputStream.write(symbol)
        }
        inputStream.close()
        bitStream.close()
        outputStream.close()

        return outputStream.toByteArray()
    }

    /**
     * Maps a prefix marker byte to a prefix string.
     */
    private fun prefixToText(marker: Byte): String {
        return when (marker) {
            PREFIX_ONE_MARKER -> PREFIX_ONE
            PREFIX_TWO_MARKER -> PREFIX_TWO
            else -> error("unknown prefix marker $marker")
        }
    }

    /**
     * Maps a suffix marker byte to a suffix string.
     */
    private fun suffixToText(marker: Byte): String {
        return when (marker) {
            SUFFIX_ONE_MARKER -> SUFFIX_ONE
            SUFFIX_TWO_MARKER -> SUFFIX_TWO
            SUFFIX_THREE_MARKER -> SUFFIX_THREE
            else -> error("unknown suffix marker $marker")
        }
    }

    companion object {

        /*
         * Each image source has a prefix. All images of a row have the same prefix.
         */

        const val PREFIX_ONE_MARKER: Byte = 0
        const val PREFIX_ONE: String =
            "https://static.planetminecraft.com/files/resource_media/screenshot/"
        const val PREFIX_TWO_MARKER: Byte = 1
        const val PREFIX_TWO: String =
            "https://static.planetminecraft.com/files/image/minecraft/project"


        /**
         * Each row has its own string which gets represented by this marker. Therefore, we achieve
         * an up zo 20% high compression ratio compared to a normal Huffman encoding.
         */
        const val COMPRESSION_MARKER: Byte = 123

        /*
         * Each image has suffix, normally a file ending.
         */

        const val SUFFIX_ONE_MARKER: Byte = 124
        const val SUFFIX_ONE: String = ".png"
        const val SUFFIX_TWO_MARKER: Byte = 125
        const val SUFFIX_TWO: String = ".jpg"
        const val SUFFIX_THREE_MARKER: Byte = 126
        const val SUFFIX_THREE: String = "_l.webp"
    }
}
