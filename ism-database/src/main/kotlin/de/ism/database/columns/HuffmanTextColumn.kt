package de.ism.database.columns

import de.ism.database.huffman.CodeTree
import de.ism.database.tables.init.CapacityFactor

/**
 * Stores textual values compressed by the Huffman code.
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class HuffmanTextColumn(
    table: Array<ByteArray>,
    codeTree: CodeTree,
    capacityFactor: CapacityFactor,
) : HuffmanColumn<String>(table, codeTree, capacityFactor) {

    override fun convert(array: ByteArray): String {
        return array.decodeToString()
    }
}
