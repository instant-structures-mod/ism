package de.ism.database.columns

/**
 * A column of a relational database that represents an ordered set of data values of a specific
 * type.
 *
 * Created on 2020.12.31 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
typealias IColumn<E> = List<E>
