package de.ism.database.columns

import de.ism.database.base.IUri
import de.ism.database.base.Uri

/**
 * Holds the creators' sources.
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class CreatorSourceColumn(
    private val names: IColumn<String>,
) : AbstractList<IUri>(), IColumn<IUri> {

    override val size: Int
        get() = names.size

    override fun get(index: Int): IUri {
        return Uri(PREFIX + creatorToUri(names[index]))
    }

    companion object {

        const val PREFIX = "https://www.planetminecraft.com/member/"

        fun creatorToUri(name: String): String {
            return name.lowercase()
                .replace(' ', '_')
                .replace("[", "")
                .replace("]", "")
                .replace("(", "")
                .replace(")", "")
                .replace(".", "")
                .replace("+", "")
                .replace("@", "")
        }
    }
}
