package de.ism.database.columns

import de.ism.database.keys.TagKey

/**
 * Holds sets of tag keys.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class TagKeysColumn(
    val table: Array<ByteArray>,
) : AbstractList<Set<TagKey>>(), IColumn<Set<TagKey>> {

    override val size: Int
        get() = table.size

    override fun get(index: Int): Set<TagKey> {
        val row = table[index]
        val size = row.size / 2
        val tags = mutableSetOf<TagKey>()

        for (entry in 0 until size) {
            val first = (row[2 * entry + 0].toInt() and 0xFF).shl(8)
            val second = row[2 * entry + 1].toInt() and 0xFF
            val tag = first or second
            tags.add(tag)
        }
        return tags
    }
}
