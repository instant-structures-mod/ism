package de.ism.database.columns

import de.ism.database.base.IUri
import de.ism.database.base.Uri
import de.ism.database.huffman.CodeTree
import de.ism.database.tables.init.CapacityFactor

/**
 * Holds the source of a structure. For now, source are always schematic files.
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>ﬂ
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class StructureSourceColumn(
    table: Array<ByteArray>,
    codeTree: CodeTree,
    capacityFactor: CapacityFactor,
) : HuffmanColumn<IUri>(table, codeTree, capacityFactor) {

    override fun convert(array: ByteArray): IUri {
        return Uri(PREFIX + array.decodeToString() + SUFFIX)
    }

    companion object {
        const val PREFIX: String =
            "https://s3.amazonaws.com/static.planetminecraft.com/files/resource_media/schematic/"
        const val SUFFIX: String = ".schematic"
    }
}
