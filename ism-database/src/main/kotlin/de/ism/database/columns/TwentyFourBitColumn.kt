package de.ism.database.columns

/**
 * Holds numerical values that require a resolution of 24 bits.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class TwentyFourBitColumn(
    private val table: ByteArray,
) : AbstractList<Int>(), IColumn<Int> {

    override val size: Int
        get() = table.size / 3

    override fun get(index: Int): Int {
        return (table[3 * index].toInt() and 0xFF).shl(16) or
            (table[3 * index + 1].toInt() and 0xFF).shl(8) or
            (table[3 * index + 2].toInt() and 0xFF)
    }
}
