package de.ism.database.columns

import de.ism.database.huffman.BitInputStream
import de.ism.database.huffman.CodeTree
import de.ism.database.huffman.HuffmanDecompress
import de.ism.database.tables.init.CapacityFactor
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import kotlin.math.roundToInt

/**
 * Stores values compressed by the Huffman compression algorithm.
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class HuffmanColumn<E>(
    /**
     * Array of compressed rows.
     */
    protected val table: Array<ByteArray>,
    /**
     * Tree to decode a compressed row.
     */
    protected val codeTree: CodeTree,
    /**
     * The maximum compression rate of a row. Used to calculate a fitting array size to prevent
     * unnecessary copy operations.
     */
    protected val capacityFactor: CapacityFactor,
) : AbstractList<E>(), IColumn<E> {

    override val size: Int
        get() = table.size

    abstract fun convert(array: ByteArray): E

    override fun get(index: Int): E {
        return convert(decode(table[index]))
    }

    open fun decode(byteArray: ByteArray): ByteArray {
        val inputStream = ByteArrayInputStream(byteArray)
        val bitStream = BitInputStream(inputStream)
        val capacity = (byteArray.size * capacityFactor).roundToInt()
        val outputStream = ByteArrayOutputStream(capacity)

        while (true) {
            val symbol = HuffmanDecompress.read(codeTree, bitStream)
            // EOF symbol
            if (symbol == 256 || symbol == ';'.code) {
                break
            }
            outputStream.write(symbol)
        }

        return outputStream.toByteArray()
    }
}
