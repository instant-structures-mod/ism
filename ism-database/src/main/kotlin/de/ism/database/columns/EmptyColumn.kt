package de.ism.database.columns

/**
 * Column that returns a default for each index.
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class EmptyColumn<E>(
    private val default: E,
    override val size: Int,
) : AbstractList<E>(), IColumn<E> {

    override fun get(index: Int): E {
        return default
    }
}
