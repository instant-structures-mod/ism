package de.ism.database.columns

/**
 * Stores one part of dimension (width, height, length) with a minimum value of 1 and maximum of
 * 256.
 * <p>
 * created on 2022.05.04 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class DimColumn(
    private val table: ByteArray,
) : AbstractList<Int>(), IColumn<Int> {

    override val size: Int
        get() = table.size

    override fun get(index: Int): Int {
        return (table[index].toInt() and 0xFF) + 1
    }
}
