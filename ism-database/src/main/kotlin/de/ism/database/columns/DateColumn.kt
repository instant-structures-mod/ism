package de.ism.database.columns

import java.time.LocalDate

/**
 * Stores dates beginning from 2000 and later.
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class DateColumn(
    private val table: ByteArray,
) : AbstractList<LocalDate>(), IColumn<LocalDate> {

    override val size: Int
        get() = table.size / 2

    override fun get(index: Int): LocalDate {
        val first = table[2 * index + 0].toInt()
        val second = table[2 * index + 1].toInt()

        return convert(first, second)
    }

    companion object {

        fun convert(first: Int, second: Int): LocalDate {
            val year = first.shr(1) + 2000
            val dayOfMonth = second and 31
            val month = (first and 1).shl(3) or (second.shr(5) and 7)
            return LocalDate.of(year, month, dayOfMonth)
        }
    }
}
