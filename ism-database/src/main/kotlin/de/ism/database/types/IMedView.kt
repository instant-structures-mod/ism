package de.ism.database.types

/**
 * View on the information of an entity, as well as keys to its connected ones.
 *
 * Instances of this marker interface provide read-only access to the information of an entity as a
 * whole. They further store the keys of associated entities.
 *
 * Implementations should provide a method to transform this view to a [IMaxView].
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IMedView : IMinView
