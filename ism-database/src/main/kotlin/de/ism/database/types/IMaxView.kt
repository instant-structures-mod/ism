package de.ism.database.types

/**
 * View on the information of an entity, as well as references to the views of other entities.
 *
 * Instances of this marker interface provide read-only access to the information of an entity as a
 * whole. They further store references to the views of associated entities.
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IMaxView : IMedView
