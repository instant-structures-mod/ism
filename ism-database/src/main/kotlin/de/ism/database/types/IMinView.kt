package de.ism.database.types

/**
 * View on the minimal set of information to represent a database entry.
 *
 * Instances of this marker interface provide read-only access to the identity information of
 * a database entry. This is at least the key of the entity and can be values bound to its
 * identity, for example its name or Uri.
 *
 * Implementations should provide a method to transform this view to a [IMedView] and a [IMaxView].
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IMinView : IDataView
