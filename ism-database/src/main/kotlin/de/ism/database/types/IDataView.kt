package de.ism.database.types

/**
 * View on combined database entries.
 *
 * Instances of this marker interface store read-only values of a database-like structure. There are
 * additional marker interface to define views of different granularity:
 * 1. [IMinView], represents the identity of the entity
 * 2. [IMedView], represents all information of the entity and keys to other entities
 * 3. [IMaxView], represents all information of the entity and references to the views of other
 *    entities.
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IDataView
