package de.ism.database.query

/**
 * An iterator that provides a subsequence of elements with each iteration step. With each iteration
 * step the caller might specify how many elements the next subsequence should have. The iterator
 * returns a sequence of at most the specified length. If the underlying sequence has no more
 * elements the returned subsequence is shorter. Implementations might filter the elements of the
 * underlying sequence.
 *
 * Created on 2020.12.30 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface ISubsequenceIterator<out R> {

    /**
     * Provides the next subsequence in the iteration with the specified size as an upper bound. The
     * selection may not include all elements of the sequence.
     *
     * @param subsequenceSize inclusive upper bound of the returned subsequence
     * @return the next subsequence. Returns an empty list if the underlying sequence does not
     * contain more fitting elements.
     */
    fun next(subsequenceSize: Int): List<R>
}
