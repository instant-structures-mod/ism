package de.ism.database.query

/**
 * Defines the sorting order.
 *
 * Created on 2020.12.28 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
enum class ESortingOrder {

    /**
     * Sort entries in rising order, for example, `1, 2, 3...` or `A, B, C...`.
     */
    ASCENDING,

    /**
     * Sort entries in downwarding order, for example `100, 99, 98...` or `Z, Y, X...`.
     */
    DESCENDING
}
