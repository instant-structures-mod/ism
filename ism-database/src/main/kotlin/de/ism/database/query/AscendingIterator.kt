package de.ism.database.query

import java.util.function.Function

/**
 * [Evaluating iterator][EvaluatingIterator] that iterates from *the 0th to the last index* of the
 * provided collection.
 *
 * Created on 2020.12.30 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
class AscendingIterator<out R>(
    evaluator: Function<Int, Boolean>,
    producer: Function<Int, R>,
    sequenceSize: Int,
) : EvaluatingIterator<R>(
    evaluator,
    producer,
    firstIndex = -1,
    lastIndex = sequenceSize - 1,
    step = 1
) {
    init {
        if (sequenceSize < 1) {
            throw IllegalArgumentException("the underlying sequence must have at least one element")
        }
    }
}
