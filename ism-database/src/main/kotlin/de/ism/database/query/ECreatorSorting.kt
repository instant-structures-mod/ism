package de.ism.database.query

/**
 * Sorting categories for creators.
 *
 * Created on 2020.12.28 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
enum class ECreatorSorting {

    /**
     * Sort creators by a popularity metric.
     */
    POPULARITY,

    /**
     * Sort creators by their number of views.
     */
    VIEWS,

    /**
     * Sort creators by their number of projects.
     */
    PROJECTS
}
