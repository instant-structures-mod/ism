package de.ism.database.query

/**
 * Sorting categories for projects.
 *
 * Created on 2020.12.28 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
enum class EProjectSorting {

    /**
     * Sort projects by a popularity metric.
     */
    POPULARITY,

    /**
     * Sort projects by their publication date.
     */
    PUBLICATION_DATE,

    /**
     * Sort projects by their optional upgrade date. If none is provided the publication date is
     * used instead.
     */
    UPGRADE_DATE,

    /**
     * Sort projects by their number of views.
     */
    VIEWS,

    /**
     * Sort projects by their number of downloads.
     */
    DOWNLOADS,

    /**
     * Sort projects by their number of favorites.
     */
    FAVORITES
}
