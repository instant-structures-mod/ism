package de.ism.database.query

import java.util.function.Function

/**
 * [Subsequence iterator][ISubsequenceIterator] that iterates over a specified range of index-able
 * collection.
 *
 * Created on 2020.12.31 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
open class EvaluatingIterator<out R>(

    /**
     * Checks if the element of the given index should be included to the subsequence.
     */
    private val evaluator: Function<Int, Boolean>,

    /**
     * Maps an index to a value of the return type.
     */
    private val producer: Function<Int, R>,

    /**
     * Inclusive index to start processing the subsequence.
     */
    firstIndex: Int,

    /**
     * Inclusive index to stop processing the subsequence.
     */
    private val lastIndex: Int,

    /**
     * Must be greater than `Int.MIN_VALUE` and not equal to zero.
     */
    private val step: Int,
) : ISubsequenceIterator<R> {

    init {
        if (step == Int.MIN_VALUE || step == 0) {
            throw IllegalArgumentException("illegal step value $step")
        }
    }

    private var currentIndex = firstIndex

    override fun next(subsequenceSize: Int): List<R> {

        val elements: MutableList<R> = ArrayList(subsequenceSize)

        @Suppress("FromClosedRangeMigration")
        val progression = IntProgression.fromClosedRange(
            rangeStart = currentIndex + step,
            rangeEnd = lastIndex,
            step = step
        )
        for (index in progression) {
            if (evaluator.apply(index)) {
                elements.add(producer.apply(index))
            }

            currentIndex = index
            if (elements.size == subsequenceSize) {
                return elements
            }
        }
        return elements
    }
}
