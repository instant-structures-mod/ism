package de.ism.database.views

import de.ism.database.base.EMinecraftVersion

/**
 * Instances contain the combined information of one or multiple structures.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IAggregatedStructures {

    /**
     * Cached structure instances
     *
     * @return list of cached structures
     */
    fun getStructures(): List<IMedStructureView>

    /**
     * Provides the sum over all blocks of the structures.
     *
     * @return summed structure blocks
     */
    fun getBlocks(): Int

    /**
     * Provides the minimal Minecraft version of all structures.
     *
     * @return lowest Minecraft version
     */
    fun getLowestMinecraftVersion(): EMinecraftVersion

    /**
     * Provides the maximal Minecraft version of all structures.
     *
     * @return highest Minecraft version
     */
    fun getHighestMinecraftVersion(): EMinecraftVersion
}
