package de.ism.database.views

import de.ism.database.base.ISchematic
import de.ism.database.types.IMaxView

/**
 * [Max view][de.ism.database.types.IMaxView] on a structure entity. Instances contain a
 * reference to their creator and project, as well as to the consumed structure file.
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMaxStructureView : IMedStructureView, IMaxView {

    override fun toMinStructureView(): IMinStructureView

    override fun toMedStructureView(): IMedStructureView

    override fun toMaxStructureView(): IMaxStructureView = this

    /**
     * Provides the entity that built this structure.
     *
     * @return creator of this structure
     */
    fun getCreator(): IMedCreatorView

    /**
     * Provides the project this structure belongs to.
     *
     * @return project of this structure
     */
    fun getProject(): IMedProjectView

    /**
     * Provides the physical structure.
     *
     * @return physical structure of this file
     */
    fun getSchematic(): ISchematic
}
