package de.ism.database.views

import de.ism.database.base.IImage
import de.ism.database.keys.ProjectKey
import de.ism.database.types.IMedView

/**
 * [View][de.ism.database.types.IMedView] on a creator entity.
 *
 * Instances mirror a internet profile of such entity with optional [profile image][getProfileImage]
 * and [banner image][getBannerImage] to that profile. Published projects can be accessed through
 * [project keys][getProjectKeys].
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMedCreatorView : IMinCreatorView, IMedView {

    override fun toMinCreatorView(): IMinCreatorView

    override fun toMedCreatorView(): IMedCreatorView = this

    override fun toMaxCreatorView(): IMaxCreatorView

    /**
     * Unique identifiers of the owned projects
     *
     * @return references to the owned projects.
     */
    fun getProjectKeys(): List<ProjectKey>

    /**
     * Avatar image of the creator's online profile
     *
     * @return an avatar image of the creator.
     */
    fun getProfileImage(): IImage?

    /**
     * Background image of the profile title.
     *
     * @return a background image of the profile title.
     */
    fun getBannerImage(): IImage?

    /**
     * Times the creator's profile was viewed.
     *
     * A creator has a ISM-intern profile. This number shows how often the profile was viewed.
     *
     * @return the number of views on the ISM-intern creator page.
     */
    fun getViews(): Int
}
