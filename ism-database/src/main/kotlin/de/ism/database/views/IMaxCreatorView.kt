package de.ism.database.views

import de.ism.database.types.IMaxView

/**
 * [Max view][de.ism.database.types.IMaxView] on a creator entity. Contains aggregated data of
 * projects and [medium views][IMedProjectView] on them.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMaxCreatorView : IMedCreatorView, IMaxView {

    override fun toMinCreatorView(): IMinCreatorView

    override fun toMedCreatorView(): IMedCreatorView

    override fun toMaxCreatorView(): IMaxCreatorView = this

    /**
     * Provides a combined view and medium views on the contained projects.
     *
     * @return combined and aggregated view
     */
    fun getAggregation(): IAggregatedProjects
}
