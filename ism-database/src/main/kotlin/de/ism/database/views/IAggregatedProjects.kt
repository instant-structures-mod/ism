package de.ism.database.views

import java.time.LocalDate

/**
 * Instances contain the combined information of one or multiple projects.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IAggregatedProjects {

    /**
     * Provides the projects whose information get combined.
     *
     * @return [medium view][de.ism.database.types.IMedView] on the projects
     */
    fun getProjects(): List<IMedProjectView>

    /**
     * Union over all tags of the projects.
     *
     * @return combined tags
     */
    fun getTags(): Set<String>

    /**
     * Provides the earliest date the creator published a project.
     *
     * This date is the minimum publication date of creator's projects that this database stores.
     *
     * @return first publication date
     */
    fun getFirstPublished(): LocalDate

    /**
     * Provides the latest date the creator published a project.
     *
     * This date is the maximum publication date of creator's projects that this database stores.
     *
     * @return last update
     */
    fun getLastPublished(): LocalDate

    /**
     * Provides the optional latest date the creator updated a project.
     *
     * This date is the maximum update of creator's projects that this database stores.
     *
     * @return last update
     */
    fun getLastUpgraded(): LocalDate?

    /**
     * Provides the sum over all project views of the creator.
     *
     * This number qualifies how many times ISM users viewed his or her projects pages.
     *
     * @return summed project views
     */
    fun getViews(): Int

    /**
     * Provides the sum over all project downloads of the creator.
     *
     * This number qualifies how many times ISM users downloaded his or her projects.
     *
     * @return summed project views
     */
    fun getDownloads(): Int

    /**
     * Provides the sum over all project favorites of the creator.
     *
     * This number qualifies how many times ISM users added his or her projects to their favorites.
     *
     * @return summed project views
     */
    fun getFavorites(): Int
}
