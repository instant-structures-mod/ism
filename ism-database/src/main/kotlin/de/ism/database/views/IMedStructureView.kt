package de.ism.database.views

import de.ism.database.base.Dim
import de.ism.database.base.EMinecraftVersion
import de.ism.database.base.IUri
import de.ism.database.keys.ProjectKey
import de.ism.database.types.IMedView

/**
 * [View][de.ism.database.types.IMedView] on a structure entity.
 *
 * A structure consist of a [reference][getResource] to the physical structure. It further includes
 * metadata of this physical structure:
 * * [dimension][getDim]
 * * [quantity of blocks][getBlockQuantity]
 * * [Minecraft version][getMinecraftVersion] required for fully placing this structure.
 * The project that contains this structure can be accessed through its [key][getProjectKey].
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMedStructureView : IMinStructureView, IMedView {

    override fun toMinStructureView(): IMinStructureView

    override fun toMedStructureView(): IMedStructureView = this

    override fun toMaxStructureView(): IMaxStructureView

    /**
     * Provides the unique identifier of the project that this structure belongs to.
     *
     * @return project unique identifier
     */
    fun getProjectKey(): ProjectKey

    /**
     * Provides a path or Uri to the structure file.
     *
     * @return identifier to resolve the structure file
     */
    fun getResource(): IUri

    /**
     * Provides the dimension of the structure.
     *
     * @return 3-dimensional vector which represents the dimension of the structure
     */
    fun getDim(): Dim

    /**
     * Provides the quantity of non-air blocks the structure consist of.
     *
     * @return [volume][de.ism.database.base.Dim.volume] of the structure minus the quantity of
     *         air blocks.
     */
    fun getBlockQuantity(): Int

    /**
     * @see de.ism.database.tables.IStructureTable.getMinecraftVersion
     */
    fun getMinecraftVersion(): EMinecraftVersion
}
