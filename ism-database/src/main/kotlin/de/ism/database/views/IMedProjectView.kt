package de.ism.database.views

import de.ism.database.keys.CreatorKey
import de.ism.database.keys.StructureKey
import de.ism.database.types.IMedView
import java.net.URI
import java.time.LocalDate

/**
 * [View][de.ism.database.types.IMinView] on a project entity.
 *
 * A project consist of one more structures and common meta-data for these structures. A project has
 * [visual resources][getContent] like images or videos, [tags][getTags], a
 * [publication date][getPublicationDate] and an optional [change date][getUpgradeDate].
 * Furthermore, instances have the numerical quantities [views][getViews],
 * [downloads][getDownloads], and [favorites][getFavorites] to define their success. Contained
 * structures can be accessed through their [keys][getStructureKeys].
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("TooManyFunctions")
interface IMedProjectView : IMinProjectView, IMedView {

    override fun toMinProjectView(): IMinProjectView

    override fun toMedProjectView(): IMedProjectView = this

    override fun toMaxProjectView(): IMaxProjectView

    /**
     * Unique identifier of the creator who own this project.
     *
     * @return ISM-intern UID of the creator of this project.
     */
    fun getCreatorKey(): CreatorKey

    /**
     * Unique identifiers of the structures this project has.
     *
     * @return ISM-intern UIDs of contained structures.
     */
    fun getStructureKeys(): List<StructureKey>

    /**
     * Visual resources of this project.
     *
     * @return path or Uri to images or videos
     */
    fun getContent(): List<URI>

    /**
     * Keywords for this project subscribed to.
     *
     * Project have an arbitrary number of keywords. These keywords consist of lowercase latin
     * characters.
     *
     * @return keywords of this project.
     */
    fun getTags(): Set<String>

    /**
     * Name of the creator of this project.
     *
     * @return creator name
     */
    fun getCreatorName(): String

    /**
     * Date when this project was first published.
     *
     * @return publication date
     */
    fun getPublicationDate(): LocalDate

    /**
     * Date when this project was last updated.
     *
     * The value is absent if this project never received an update.
     *
     * @return date of the last update
     */
    fun getUpgradeDate(): LocalDate?

    /**
     * Measurement many times the ISM-intern project page was viewed.
     *
     * @return how many times this project was viewed.
     */
    fun getViews(): Int

    /**
     * Measurement how often the structures of this project were downloaded by ISM users.
     *
     * @return how many times the structures of this project were downloaded.
     */
    fun getDownloads(): Int

    /**
     * Measurement how often ISM users added this project to their favorites.
     *
     * @return how many times this project was added as favorite.
     */
    fun getFavorites(): Int
}
