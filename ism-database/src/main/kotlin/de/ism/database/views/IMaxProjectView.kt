package de.ism.database.views

import de.ism.database.types.IMaxView

/**
 * [Max view][de.ism.database.types.IMaxView] on a project entity. Contains aggregated data of
 * structures and [medium views][IMedProjectView] on them.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMaxProjectView : IMedProjectView, IMaxView {

    override fun toMinProjectView(): IMinProjectView

    override fun toMedProjectView(): IMedProjectView

    override fun toMaxProjectView(): IMaxProjectView = this

    /**
     * Provides a combined view and medium views on the contained structures.
     *
     * @return combined and aggregated view
     */
    fun getAggregation(): IAggregatedStructures
}
