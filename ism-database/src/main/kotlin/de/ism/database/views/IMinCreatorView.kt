package de.ism.database.views

import de.ism.database.base.IUri
import de.ism.database.keys.CreatorKey
import de.ism.database.types.IMinView

/**
 * [Minimal view][de.ism.database.types.IMinView] on a creator entity.
 *
 * A creator represents a natural person or a group of natural persons that build at least one
 * project that this database contains. The creator‘s identity is bound to its [key][getCreatorKey].
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IMinCreatorView : IMinView {

    /**
     * Transforms this creator view to [IMinCreatorView].
     */
    fun toMinCreatorView(): IMinCreatorView = this

    /**
     * Transforms this creator view to [IMedCreatorView].
     */
    fun toMedCreatorView(): IMedCreatorView

    /**
     * Transforms this creator view to [IMaxCreatorView].
     */
    fun toMaxCreatorView(): IMaxCreatorView

    /**
     * Unique identifier of this instance
     *
     * @return ISM-intern UID of this creator.
     */
    fun getCreatorKey(): CreatorKey

    /**
     * Uri to the online profile
     *
     * @return a Uri to the creator online profile.
     */
    fun getSource(): IUri

    /**
     * Title of the creator's online profile.
     *
     * @return the name of the creator. For example the title of the profile.
     */
    fun getName(): String
}
