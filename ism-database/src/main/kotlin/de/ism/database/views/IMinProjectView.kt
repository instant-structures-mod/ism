package de.ism.database.views

import de.ism.database.base.IUri
import de.ism.database.keys.ProjectKey
import de.ism.database.types.IMinView

/**
 * [Minimal view][de.ism.database.types.IMinView] on a project entity.
 *
 * A project represents metadata of one or more structures. This metadata is not bound to the
 * physical structure. The projects‘s identity is bound to its [key][getProjectKey].
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMinProjectView : IMinView {

    /**
     * Transforms this project view to [IMinProjectView].
     */
    fun toMinProjectView(): IMinProjectView = this

    /**
     * Transforms this project view to [IMedProjectView].
     */
    fun toMedProjectView(): IMedProjectView

    /**
     * Transforms this project view to [IMaxProjectView].
     */
    fun toMaxProjectView(): IMaxProjectView

    /**
     * Unique identifier of this instance
     *
     * @return ISM-intern UID of this project
     */
    fun getProjectKey(): ProjectKey

    /**
     * Uri to the website or blog entry of this project.
     *
     * @return Uri to access the source of this project.
     */
    fun getSource(): IUri

    /**
     * Title of this project.
     *
     * @return the name of this project
     */
    fun getName(): String
}
