package de.ism.database.views

import de.ism.database.keys.StructureKey
import de.ism.database.types.IMinView

/**
 * [Minimal view][de.ism.database.types.IMinView] on a structure entity.
 *
 * A structures view represents a structure whose blocks can be placed inside a Minecraft world. The
 * physical structure normally gets stored in a schematic file. Such view provides read-only access
 * to the meta data that is bound to the physical structure. If the physical structure changes, the
 * meta changes too. The structures‘s identity is bound to its [key][getStructureKey].
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

interface IMinStructureView : IMinView {

    /**
     * Transforms this structure view to [IMinCreatorView].
     */
    fun toMinStructureView(): IMinStructureView = this

    /**
     * Transforms this structure view to [IMedCreatorView].
     */
    fun toMedStructureView(): IMedStructureView

    /**
     * Transforms this structure view to [IMaxCreatorView].
     */
    fun toMaxStructureView(): IMaxStructureView

    /**
     * Provides the unique identifier of this instance.
     *
     * @return ISM-intern UID of the represented structure entity
     */
    fun getStructureKey(): StructureKey

    /**
     * Provides an optional name of this structure entity.
     *
     * A name can be made up from any characters allowed in a filename. It works as a fallback, if
     * additional human-readable identifiers are needed to differentiate multiple structures of one
     * project.
     *
     * @return name of the represented structure entity.
     */
    fun getName(): String?
}
