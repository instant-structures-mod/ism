package de.ism.database.huffman

/*
 * Reference Huffman coding
 * Copyright (c) Project Nayuki
 *
 * https://www.nayuki.io/page/reference-huffman-coding
 * https://github.com/nayuki/Reference-Huffman-coding
 */

/**
 * A leaf node in a code tree. It has a symbol value. Immutable.
 * @see CodeTree
 */
class Leaf(
    sym: Int,
) : Node {
    /**
     * Always non-negative
     */
    val symbol: Int

    init {
        require(sym >= 0) { "Symbol value must be non-negative" }
        symbol = sym
    }
}
