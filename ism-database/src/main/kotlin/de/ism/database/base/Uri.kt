package de.ism.database.base

import java.net.URI

/**
 * Created on 2021.01.23 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
class Uri(
    val content: String,
) : IUri {

    private val _uri: URI by lazy {
        URI.create(content)
    }

    override fun getUri() = _uri

    override fun openUri() {
        TODO("Not yet implemented")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Uri

        if (content != other.content) return false

        return true
    }

    override fun hashCode() = content.hashCode()

    override fun toString() = "Uri(content='$content')"
}
