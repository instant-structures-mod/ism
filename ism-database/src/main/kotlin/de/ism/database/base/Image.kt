package de.ism.database.base

import java.awt.Image
import java.awt.image.BufferedImage
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import javax.imageio.ImageIO

/**
 * Created on 2021.02.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
class Image(
    private val source: String,
) : IImage {

    private val _image: BufferedImage? by lazy {
        var connection: HttpURLConnection? = null
        try {
            connection = URL(source).openConnection() as HttpURLConnection
            connection.addRequestProperty("User-Agent", "Mozilla/5.0")
            connection.inputStream.use {
                ImageIO.read(it)
            }
        } catch (exception: IOException) {
            exception.printStackTrace()
        } finally {
            connection?.disconnect()
        }
        null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as de.ism.database.base.Image

        if (source != other.source) return false

        return true
    }

    override fun hashCode() = source.hashCode()

    /**
     * Returns the source of this image.
     */
    fun getSource() = source

    override fun getImage(): Image? = _image

    override fun scaledToWidth(width: Int): Image? {
        TODO("Not yet implemented")
    }

    override fun scaledToHeight(height: Int): Image? {
        TODO("Not yet implemented")
    }

    override fun toString() = "Image(source='$source')"

    companion object {

        /**
         * default image
         */
        @JvmField
        val DEFAULT_IMAGE = Image("")
    }
}
