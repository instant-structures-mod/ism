package de.ism.database.base

import org.jetbrains.annotations.Contract

/**
 * Placeholder implementation for future use.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("KDocMissingDocumentation")
enum class EMinecraftVersion(
    private val version: String,
) : Comparable<EMinecraftVersion> {

    DEFAULT("default"),
    MC_1_7_10("1.7.10"),
    MC_1_8_0("1.8.0"),
    MC_1_8_8("1.8.8"),
    MC_1_8_9("1.8.9"),
    MC_1_9_0("1.9.0"),
    MC_1_9_4("1.9.4"),
    MC_1_10_0("1.10.0"),
    MC_1_10_2("1.10.2"),
    MC_1_11_0("1.11.0"),
    MC_1_11_2("1.11.2"),
    MC_1_12_0("1.12.0"),
    MC_1_12_1("1.12.1"),
    MC_1_12_2("1.12.2"),
    MC_1_13_2("1.13.2"),
    MC_1_14_2("1.14.2"),
    MC_1_14_3("1.14.3"),
    MC_1_14_4("1.14.4"),
    MC_1_15_0("1.15.0"),
    MC_1_15_1("1.15.1"),
    MC_1_15_2("1.15.2"),
    MC_1_16_1("1.16.1"),
    MC_1_16_2("1.16.2"),
    MC_1_16_3("1.16.3"),
    MC_1_16_4("1.16.4"),
    MC_1_16_5("1.16.5"),
    MC_1_17_1("1.17.1"),
    MC_1_18_0("1.18.0"),
    MC_1_18_1("1.18.1"),
    MC_1_18_2("1.18.2"),
    DEV("Dev1.18.2");

    @Contract(pure = true)
    override fun toString(): String = "MinecraftVersion: $version"

    companion object {

        private val versionMap: Map<String, EMinecraftVersion>

        init {
            val map: MutableMap<String, EMinecraftVersion> = HashMap(values().size)

            for (value in values()) {
                map[value.version] = value
            }
            versionMap = map
        }

        /**
         * Returns the minecraft version specified by the given ID.
         */
        fun fromVersion(version: String): EMinecraftVersion =
            versionMap.getOrDefault(version, DEFAULT)
    }
}
