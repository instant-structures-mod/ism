package de.ism.database.base

import java.awt.Image

/**
 * Provides an [Image] with convenience functions.
 *
 * Created on 2020.12.31 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IImage {

    /**
     * Returns the stored image in its original size.
     */
    fun getImage(): Image?

    /**
     * Returns the stored image proportionally scaled to the specified width.
     */
    fun scaledToWidth(width: Int): Image?

    /**
     * Returns the stored image proportionally scaled to the specified height.
     */
    fun scaledToHeight(height: Int): Image?
}
