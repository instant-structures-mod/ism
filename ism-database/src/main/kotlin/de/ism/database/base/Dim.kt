package de.ism.database.base

/**
 * Placeholder implementation for future use.
 *
 * Created on 2020.12.26 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
data class Dim(
    private val dimX: Int,
    private val dimY: Int,
    private val dimZ: Int,
) {

    /**
     * Volume of this structure.
     */
    fun volume(): Int = dimX * dimY * dimZ

    companion object {

        /**
         * default dimension
         */
        @JvmField
        val DEFAULT_DIM = Dim(0, 0, 0)
    }
}
