package de.ism.database.base

import java.net.URI

/**
 * Provides and opens an [URI].
 *
 * Created on 2021.01.01 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IUri {

    /**
     * Returns the stored URI.
     */
    fun getUri(): URI

    /**
     * Opens the stored URI in the standard browser.
     */
    fun openUri()
}
