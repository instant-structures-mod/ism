package de.ism.database.tables

import de.ism.database.base.Dim
import de.ism.database.base.EMinecraftVersion
import de.ism.database.base.IUri
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.keys.StructureKey
import de.ism.database.views.IMaxStructureView
import de.ism.database.views.IMedStructureView
import de.ism.database.views.IMinStructureView

/**
 * Represents the structure table of a relational database.
 *
 * Created on 2020.12.29 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface IStructureTable : ITable {

    /**
     * Returns a [IMinStructureView] from the given key.
     */
    fun getMinStructure(key: CreatorKey): IMinStructureView

    /**
     * Returns a [IMedStructureView] from the given key.
     */
    fun getMedStructure(key: CreatorKey): IMedStructureView

    /**
     * Returns a [IMaxStructureView] from the given key.
     */
    fun getMaxStructure(key: CreatorKey): IMaxStructureView

    /**
     * Returns the optional name of the [given structure][key].
     */
    fun getName(key: StructureKey): String?

    /**
     * Returns the key of the project containing the [given structure][key].
     */
    fun getProjectKey(key: StructureKey): ProjectKey

    /**
     * Returns the [IUri] to the file of the [given structure][key].
     */
    fun getResource(key: ProjectKey): IUri

    /**
     * Returns the dimension of the [given structure][key].
     */
    fun getDim(key: StructureKey): Dim

    /**
     * Returns the number of blocks of the [given structure][key].
     */
    fun getBlocks(key: StructureKey): Int

    /**
     * Returns the minimum Minecraft version required to place this structure correctly.
     *
     * Each structure consist of a different blocks. Newer Minecraft versions introduce newer
     * blocks. The returned version is the first one that contains all blocks. But this does not
     * mean, older Minecraft version cannot place this structure. Not available blocks can be
     * replaced by other ones.
     *
     * @return minimum Minecraft version to place all blocks of this structure.
     */
    fun getMinecraftVersion(key: StructureKey): EMinecraftVersion
}
