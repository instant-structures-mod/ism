package de.ism.database.tables

import de.ism.database.keys.ProjectKey

/**
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
interface ITagTable {

    fun getStem(index: Int): String

    fun getFullText(index: Int): String

    fun getProjectKeys(index: Int): Set<ProjectKey>
}
