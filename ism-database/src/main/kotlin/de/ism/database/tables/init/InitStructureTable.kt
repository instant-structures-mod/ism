package de.ism.database.tables.init

import de.ism.database.PathUtil
import de.ism.database.base.EMinecraftVersion
import de.ism.database.base.IUri
import de.ism.database.columns.*
import de.ism.database.keys.ProjectKey
import de.ism.database.tables.StructureTable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.nio.file.Files
import java.nio.file.Path

object InitStructureTable {

    val path: Path = PathUtil.assetsPath.resolve("structures/")
    var meta: StructureMeta = let {
        val path = path.resolve("meta.json")
        if (Files.exists(path)) {
            Json.decodeFromStream(Files.newInputStream(path))
        } else {
            StructureMeta()
        }
    }

    fun loadTable(): StructureTable {
        return StructureTable(
            loadSource(),
            loadName(),
            loadProjectKey(),
            loadBlocks(),
            loadDimX(),
            loadDimY(),
            loadDimZ(),
            loadMinecraftVersion(),
        )
    }

    fun loadSource(): IColumn<IUri> {
        val path = path.resolve("source")
        val table = InitTables.readTwoByteSizedTable(path, meta.size)
        val tree = InitTables.readTree(path)
        return StructureSourceColumn(table, tree, meta.sourceFactor)
    }

    fun loadName(): IColumn<String?> {
        return EmptyColumn<String?>(null, meta.size)
    }

    fun loadProjectKey(): IColumn<ProjectKey> {
        val path = path.resolve("project_key")
        val bytes = Files.readAllBytes(path)
        return object : SixteenBitColumn<ProjectKey>(bytes) {

            override fun convert(first: Byte, second: Byte): ProjectKey {
                val partOne = (first.toInt() and 0xFF).shl(8)
                val partTwo = second.toInt() and 0xFF
                return partOne or partTwo
            }
        }
    }

    fun loadBlocks(): IColumn<Int> {
        val path = path.resolve("blocks")
        val bytes = Files.readAllBytes(path)
        return TwentyFourBitColumn(bytes)
    }

    fun loadDimX(): IColumn<Int> {
        val path = path.resolve("dim_x")
        val bytes = Files.readAllBytes(path)
        return DimColumn(bytes)
    }

    fun loadDimY(): IColumn<Int> {
        val path = path.resolve("dim_y")
        val bytes = Files.readAllBytes(path)
        return DimColumn(bytes)
    }

    fun loadDimZ(): IColumn<Int> {
        val path = path.resolve("dim_z")
        val bytes = Files.readAllBytes(path)
        return DimColumn(bytes)
    }

    fun loadMinecraftVersion(): IColumn<EMinecraftVersion> {
        return EmptyColumn(EMinecraftVersion.DEFAULT, meta.size)
    }
}
