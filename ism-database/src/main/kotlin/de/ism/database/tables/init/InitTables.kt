package de.ism.database.tables.init

import de.ism.database.huffman.BitInputStream
import de.ism.database.huffman.CanonicalCode
import de.ism.database.huffman.CodeTree
import java.io.FileInputStream
import java.nio.file.Path

/**
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

typealias CapacityFactor = Float

object InitTables {

    fun readTree(path: Path): CodeTree {
        val treePath = path.parent.resolve("${path.fileName}_tree")
        val input = FileInputStream(treePath.toFile())
        val treeInput = BitInputStream(input)
        return readCodeLengthTable(treeInput).toCodeTree()
    }

    fun readCodeLengthTable(input: BitInputStream): CanonicalCode {
        val codeLengths = IntArray(257)
        for (i in codeLengths.indices) {
            // For this file format, we read 8 bits in big endian
            var value = 0
            repeat(8) {
                value = value shl 1 or input.readNoEof()
            }
            codeLengths[i] = value
        }
        return CanonicalCode(codeLengths)
    }

    fun readOneByteSizedTable(path: Path, entries: Int, multiplier: Int): Array<ByteArray> {
        val input = FileInputStream(path.toFile())
        return Array(entries) {
            val size = multiplier * input.read()
            val array = ByteArray(size)
            input.read(array)
            array
        }
    }

    fun readTwoByteSizedTable(
        path: Path,
        entries: Int,
    ): Array<ByteArray> {
        val input = FileInputStream(path.toFile())
        return Array(entries) {
            val sizeOne = input.read()
            val sizeTwo = input.read()
            val size = sizeOne.shl(8) or sizeTwo

            val array = ByteArray(size)
            input.read(array)
            array
        }
    }
}
