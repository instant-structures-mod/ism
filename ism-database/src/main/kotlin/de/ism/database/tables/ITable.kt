package de.ism.database.tables

/**
 * Represents a table of a relational database.
 *
 * Created on 2020.12.31 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface ITable {

    /**
     * Number of entries in this table.
     */
    val size: Int
}
