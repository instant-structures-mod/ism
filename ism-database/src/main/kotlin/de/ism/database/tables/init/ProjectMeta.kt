package de.ism.database.tables.init

import kotlinx.serialization.Serializable

/**
 * <p>
 * created on 2022.04.29 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
@Serializable
class ProjectMeta {

    var size: Int = 0
    var viewsMap: IntArray = IntArray(0)
    var downloadsMap: IntArray = IntArray(0)
    var favoritesMap: IntArray = IntArray(0)
    var sourceFactor: CapacityFactor = 0.0F
    var titleFactor: CapacityFactor = 0.0F
    var imagesFactor: CapacityFactor = 0.0F
}
