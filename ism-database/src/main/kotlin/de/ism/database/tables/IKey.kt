package de.ism.database.keys

/**
 * [Unique key](https://en.wikipedia.org/wiki/Unique_key) of a database-like structures.
 *
 * These typealiases are unique keys. Compatibility between different versions of
 * the ISM-database is provided. But future versions may remove entries from a table.
 *
 * Design decision: We do not implement keys as inline classes since it would break AssertJs code
 * generator. Instead, we implemented them as typealiases.
 *
 * Created on 2020.12.27 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */

typealias ProjectKey = Int
typealias CreatorKey = Int
typealias StructureKey = Int
typealias TagKey = Int
