package de.ism.database.tables.init

/**
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
@kotlinx.serialization.Serializable
class StructureMeta {
    var size: Int = 0
    var sourceFactor: CapacityFactor = 0.0F
}
