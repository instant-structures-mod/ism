package de.ism.database.tables.init

import de.ism.database.PathUtil
import de.ism.database.columns.FullTextColumn
import de.ism.database.columns.IColumn
import de.ism.database.columns.StemColumn
import de.ism.database.columns.TagKeysColumn
import de.ism.database.keys.ProjectKey
import de.ism.database.tables.TagTable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.nio.file.Files
import java.nio.file.Path

object InitTagTable {

    val path: Path = PathUtil.assetsPath.resolve("tags/")
    var meta: TagMeta = let {
        val path = path.resolve("meta.json")
        if (Files.exists(path)) {
            Json.decodeFromStream(Files.newInputStream(path))
        } else {
            TagMeta()
        }
    }

    fun loadTable(): TagTable {
        val (stems, fullTexts) = loadTags()
        val projectKeys = loadProjectKeys()
        return TagTable(stems, fullTexts, projectKeys)
    }

    fun loadTags(): Pair<StemColumn, FullTextColumn> {
        val path = path.resolve("tags")
        val table = InitTables.readOneByteSizedTable(path, meta.size, 1)
        val tree = InitTables.readTree(path)

        val stemColumn = StemColumn(table, meta.markerCode, tree, meta.tagFactor)
        val fullTextColumn = FullTextColumn(table, meta.markerCode, tree, meta.tagFactor)
        return stemColumn to fullTextColumn
    }

    fun loadProjectKeys(): IColumn<Set<ProjectKey>> {
        val path = InitTagTable.path.resolve("project_keys")
        val table = InitTables.readTwoByteSizedTable(path, meta.size)
        return TagKeysColumn(table)
    }
}
