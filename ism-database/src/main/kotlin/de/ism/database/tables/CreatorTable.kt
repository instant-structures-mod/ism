package de.ism.database.tables

import de.ism.database.base.IImage
import de.ism.database.base.IUri
import de.ism.database.columns.IColumn
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.query.ECreatorSorting
import de.ism.database.query.ESortingOrder
import de.ism.database.query.ISubsequenceIterator
import de.ism.database.views.IMaxCreatorView
import de.ism.database.views.IMedCreatorView
import de.ism.database.views.IMinCreatorView

/**
 * Created on 2020.12.31 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("TooManyFunctions", "LongParameterList")
class CreatorTable(
    private val sources: IColumn<IUri>,
    private val names: IColumn<String>,
    private val projectKeys: IColumn<Set<ProjectKey>>,
    private val profileImages: IColumn<IImage?>,
    private val bannerImages: IColumn<IImage?>,
    private val views: IColumn<Int>,
) : ICreatorTable {

    override val size: Int
        get() = names.size

    override fun query(
        searchQuery: String,
        sorting: ECreatorSorting,
        sortingOrder: ESortingOrder,
    ): ISubsequenceIterator<CreatorKey> {
        TODO("Not yet implemented")
    }

    override fun getMinCreator(key: CreatorKey): IMinCreatorView {
        TODO("Not yet implemented")
    }

    override fun getMedCreator(key: CreatorKey): IMedCreatorView {
        TODO("Not yet implemented")
    }

    override fun getMaxCreator(key: CreatorKey): IMaxCreatorView {
        TODO("Not yet implemented")
    }

    override fun getSource(key: CreatorKey): IUri = sources[key]

    override fun getName(key: CreatorKey): String = names[key]

    override fun getProjectKeys(key: CreatorKey): Set<ProjectKey> {
        return projectKeys[key]
    }

    override fun getProfileImage(key: CreatorKey): IImage? = profileImages[key]

    override fun getBannerImage(key: CreatorKey): IImage? = bannerImages[key]

    override fun getViews(key: CreatorKey): Int = views[key]
}
