package de.ism.database.tables.init

import kotlinx.serialization.Serializable

/**
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
@Serializable
class TagMeta {
    var size: Int = 0
    var tagFactor: CapacityFactor = 0.0F
    var markerCode: Int = 0
}
