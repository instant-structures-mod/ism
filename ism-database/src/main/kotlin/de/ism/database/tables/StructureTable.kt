package de.ism.database.tables

import de.ism.database.base.Dim
import de.ism.database.base.EMinecraftVersion
import de.ism.database.base.IUri
import de.ism.database.columns.IColumn
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.keys.StructureKey
import de.ism.database.views.IMaxStructureView
import de.ism.database.views.IMedStructureView
import de.ism.database.views.IMinStructureView

/**
 * Created on 2020.12.29 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("LongParameterList")
class StructureTable(
    private val resources: IColumn<IUri>,
    private val names: IColumn<String?>,
    private val projectKeys: IColumn<ProjectKey>,
    private val blocks: IColumn<Int>,
    private val dimX: IColumn<Int>,
    private val dimY: IColumn<Int>,
    private val dimZ: IColumn<Int>,
    private val minecraftVersions: IColumn<EMinecraftVersion>,
) : IStructureTable {

    override val size: Int
        get() = names.size

    override fun getMinStructure(key: CreatorKey): IMinStructureView {
        TODO("Not yet implemented")
    }

    override fun getMedStructure(key: CreatorKey): IMedStructureView {
        TODO("Not yet implemented")
    }

    override fun getMaxStructure(key: CreatorKey): IMaxStructureView {
        TODO("Not yet implemented")
    }

    override fun getName(key: StructureKey): String? = names[key]

    override fun getProjectKey(key: StructureKey): ProjectKey {
        return projectKeys[key]
    }

    override fun getResource(key: ProjectKey): IUri = resources[key]

    override fun getDim(key: StructureKey): Dim {
        return Dim(dimX[key], dimY[key], dimZ[key])
    }

    override fun getBlocks(key: StructureKey): Int = blocks[key]

    override fun getMinecraftVersion(key: StructureKey): EMinecraftVersion =
        minecraftVersions[key]
}
