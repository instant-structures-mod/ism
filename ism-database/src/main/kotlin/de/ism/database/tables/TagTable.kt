package de.ism.database.tables

import de.ism.database.columns.IColumn
import de.ism.database.keys.ProjectKey

/**
 * <p>
 * created on 2022.05.05 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class TagTable(
    private val stems: IColumn<String>,
    private val fullText: IColumn<String>,
    private val projectKeys: IColumn<Set<ProjectKey>>,
) : ITagTable {

    override fun getStem(index: Int): String {
        return stems[index]
    }

    override fun getFullText(index: Int): String {
        return fullText[index]
    }

    override fun getProjectKeys(index: Int): Set<ProjectKey> {
        return projectKeys[index]
    }
}
