package de.ism.database.tables.init

import de.ism.database.PathUtil
import de.ism.database.base.IUri
import de.ism.database.base.Image
import de.ism.database.columns.*
import de.ism.database.keys.ProjectKey
import de.ism.database.tables.CreatorTable
import de.ism.database.tables.ICreatorTable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.nio.file.Files
import java.nio.file.Path


fun main() {
    val table = InitCreatorTable.loadTable()
    println()
}

object InitCreatorTable {

    val path: Path = PathUtil.assetsPath.resolve("creators/")
    var meta: CreatorMeta = let {
        val path = path.resolve("meta.json")
        if (Files.exists(path)) {
            Json.decodeFromStream(Files.newInputStream(path))
        } else {
            CreatorMeta()
        }
    }

    fun loadTable(): ICreatorTable {
        val names = loadNames()

        return CreatorTable(
            loadSources(names),
            names,
            loadProjects(),
            loadProfileImages(),
            loadBannerImages(),
            loadViews(),
        )
    }

    fun loadNames(): IColumn<String> {
        val path = InitCreatorTable.path.resolve("names")
        val table = InitTables.readTwoByteSizedTable(path, meta.size)
        val tree = InitTables.readTree(path)
        return HuffmanTextColumn(table, tree, 1.7F)
    }

    fun loadSources(names: IColumn<String>): IColumn<IUri> {
        return CreatorSourceColumn(names)
    }

    fun loadProjects(): IColumn<Set<ProjectKey>> {
        val path = InitCreatorTable.path.resolve("project_keys")
        val table = InitTables.readOneByteSizedTable(path, meta.size, 2)
        return TagKeysColumn(table)
    }

    private fun loadProfileImages(): IColumn<Image?> {
        return EmptyColumn<Image?>(null, meta.size)
    }

    private fun loadBannerImages(): IColumn<Image?> {
        return EmptyColumn<Image?>(null, meta.size)
    }

    private fun loadViews(): IColumn<Int> {
        return EmptyColumn(0, meta.size)
    }
}
