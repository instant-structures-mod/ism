package de.ism.database.tables

import de.ism.database.base.IImage
import de.ism.database.base.IUri
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.query.ECreatorSorting
import de.ism.database.query.ESortingOrder
import de.ism.database.query.ISubsequenceIterator
import de.ism.database.views.IMaxCreatorView
import de.ism.database.views.IMedCreatorView
import de.ism.database.views.IMinCreatorView

/**
 * Represents the creator table of a relational database.
 *
 * Created on 2020.12.29 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
interface ICreatorTable : ITable {

    /**
     * Returns an iterator over the creators that match this search query.
     */
    fun query(
        searchQuery: String,
        sorting: ECreatorSorting,
        sortingOrder: ESortingOrder,
    ): ISubsequenceIterator<CreatorKey>

    /**
     * Returns a [IMinCreatorView] from the given key.
     */
    fun getMinCreator(key: CreatorKey): IMinCreatorView

    /**
     * Returns a [IMedCreatorView] from the given key.
     */
    fun getMedCreator(key: CreatorKey): IMedCreatorView

    /**
     * Returns a [IMaxCreatorView] from the given key.
     */
    fun getMaxCreator(key: CreatorKey): IMaxCreatorView

    /**
     * Returns a Uri to the online profile of the [given creator][key].
     */
    fun getSource(key: CreatorKey): IUri

    /**
     * Returns the name of the [given creator][key].
     */
    fun getName(key: CreatorKey): String

    /**
     * Returns the project keys of the [given creator][key].
     */
    fun getProjectKeys(key: CreatorKey): Set<ProjectKey>

    /**
     * Returns the optional resource of the profile image of the [given creator][key].
     */
    fun getProfileImage(key: CreatorKey): IImage?

    /**
     * Returns the optional resource of the banner image of the [given creator][key].
     */
    fun getBannerImage(key: CreatorKey): IImage?

    /**
     * Returns number of views of the [given creator's][key] profile.
     */
    fun getViews(key: CreatorKey): Int
}
