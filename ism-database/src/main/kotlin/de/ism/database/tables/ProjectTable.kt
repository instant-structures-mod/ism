package de.ism.database.tables

import de.ism.database.base.IImage
import de.ism.database.base.IUri
import de.ism.database.columns.IColumn
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.keys.StructureKey
import de.ism.database.keys.TagKey
import de.ism.database.query.EProjectSorting
import de.ism.database.query.ESortingOrder
import de.ism.database.query.ISubsequenceIterator
import de.ism.database.views.IMaxProjectView
import de.ism.database.views.IMedProjectView
import de.ism.database.views.IMinProjectView
import java.time.LocalDate

/**
 * Created on 2020.12.29 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("TooManyFunctions", "LongParameterList")
class ProjectTable(
    private val sources: IColumn<IUri>,
    private val titles: IColumn<String>,
    private val creatorKeys: IColumn<CreatorKey>,
    private val structureKeys: IColumn<Set<StructureKey>>,
    private val contents: IColumn<Set<IImage>>,
    private val tags: IColumn<Set<TagKey>>,
    private val publicationDates: IColumn<LocalDate>,
    private val upgradeDates: IColumn<LocalDate?>,
    private val views: IColumn<Int>,
    private val downloads: IColumn<Int>,
    private val favorites: IColumn<Int>,
) : IProjectTable {

    override val size: Int
        get() = titles.size

    override fun query(
        searchQuery: String,
        sorting: EProjectSorting,
        sortingOrder: ESortingOrder,
    ): ISubsequenceIterator<ProjectKey> {
        TODO("Not yet implemented")
    }

    override fun getMinProject(key: ProjectKey): IMinProjectView {
        TODO("Not yet implemented")
    }

    override fun getMedProject(key: ProjectKey): IMedProjectView {
        TODO("Not yet implemented")
    }

    override fun getMaxProject(key: ProjectKey): IMaxProjectView {
        TODO("Not yet implemented")
    }

    override fun getSource(key: ProjectKey): IUri = sources[key]

    override fun getTitle(key: ProjectKey): String = titles[key]

    override fun getCreatorKey(key: ProjectKey): CreatorKey {
        return creatorKeys[key]
    }

    override fun getStructureKeys(key: ProjectKey): Set<StructureKey> {
        return structureKeys[key].toSet()
    }

    override fun getContent(key: ProjectKey): Set<IImage> = contents[key]

    override fun getTags(key: ProjectKey): Set<TagKey> = tags[key]

    override fun getPublicationDate(key: ProjectKey): LocalDate = publicationDates[key]

    override fun getUpgradeDate(key: ProjectKey): LocalDate? = upgradeDates[key]

    override fun getViews(key: ProjectKey): Int = views[key]

    override fun getDownloads(key: ProjectKey): Int = downloads[key]

    override fun getFavorites(key: ProjectKey): Int = favorites[key]
}
