package de.ism.database.tables.init

import de.ism.database.PathUtil
import de.ism.database.base.IImage
import de.ism.database.base.IUri
import de.ism.database.columns.*
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.StructureKey
import de.ism.database.keys.TagKey
import de.ism.database.tables.ProjectTable
import de.ism.database.tables.init.InitTables.readOneByteSizedTable
import de.ism.database.tables.init.InitTables.readTree
import de.ism.database.tables.init.InitTables.readTwoByteSizedTable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate

object InitProjectTable {

    val path: Path = PathUtil.assetsPath.resolve("projects/")
    val meta: ProjectMeta by lazy {
        val path = path.resolve("meta.json")
        if (Files.exists(path)) {
            Json.decodeFromStream(Files.newInputStream(path))
        } else {
            ProjectMeta()
        }
    }

    fun loadTable(): ProjectTable {
        return ProjectTable(
            sources = loadSource(),
            titles = loadTitles(),
            creatorKeys = loadCreatorKeys(),
            structureKeys = loadStructureKeys(),
            contents = loadImages(),
            tags = loadTagKeys(),
            publicationDates = loadPublicationDate(),
            upgradeDates = loadUpgradeDate(),
            views = loadViews(),
            downloads = loadDownloads(),
            favorites = loadFavorites()
        )
    }

    fun loadTitles(): IColumn<String> {
        val path = path.resolve("title")
        val table = readTwoByteSizedTable(path, meta.size)
        val tree = readTree(path)
        return HuffmanTextColumn(table, tree, meta.titleFactor)
    }

    fun loadSource(): IColumn<IUri> {
        val path = path.resolve("source")
        val table = readTwoByteSizedTable(path, meta.size)
        val tree = readTree(path)
        return ProjectSourceColumn(table, tree, meta.sourceFactor)
    }

    fun loadCreatorKeys(): IColumn<CreatorKey> {
        val path = path.resolve("creator_key")
        val bytes = Files.readAllBytes(path)
        return object : SixteenBitColumn<CreatorKey>(bytes) {

            override fun convert(first: Byte, second: Byte): CreatorKey {
                return first.toInt().shl(8) or (second.toInt() and 0xFF)
            }
        }
    }

    fun loadStructureKeys(): IColumn<Set<StructureKey>> {
        val path = path.resolve("structure_key")
        val bytes = Files.readAllBytes(path)
        return object : SixteenBitColumn<Set<StructureKey>>(bytes) {

            override fun convert(first: Byte, second: Byte): Set<StructureKey> {
                val partOne = (first.toInt() and 0xFF).shl(8)
                val partTwo = second.toInt() and 0xFF
                return mutableSetOf(partOne or partTwo)
            }
        }
    }

    fun loadImages(): IColumn<Set<IImage>> {
        val path = path.resolve("images")
        val table = readTwoByteSizedTable(path, meta.size)
        val tree = readTree(path)
        return ImagesColumn(table, tree, meta.imagesFactor)
    }

    fun loadTagKeys(): IColumn<Set<TagKey>> {
        val path = path.resolve("tag_keys")
        val table = readOneByteSizedTable(path, meta.size, 2)
        return TagKeysColumn(table)
    }

    fun loadPublicationDate(): IColumn<LocalDate> {
        val path = path.resolve("publication_date")
        val bytes = Files.readAllBytes(path)
        return DateColumn(bytes)
    }

    fun loadUpgradeDate(): IColumn<LocalDate?> {
        val path = path.resolve("upgrade_date")
        val bytes = Files.readAllBytes(path)
        return OptionalDateColumn(bytes)
    }

    fun loadViews(): IColumn<Int> {
        val path = path.resolve("views")
        val bytes = Files.readAllBytes(path)
        return FourBitColumn(meta.viewsMap, bytes)
    }

    fun loadDownloads(): IColumn<Int> {
        val path = path.resolve("downloads")
        val bytes = Files.readAllBytes(path)
        return FourBitColumn(meta.downloadsMap, bytes)
    }

    fun loadFavorites(): IColumn<Int> {
        val path = path.resolve("favorites")
        val bytes = Files.readAllBytes(path)
        return FourBitColumn(meta.favoritesMap, bytes)
    }
}
