package de.ism.database.tables

import de.ism.database.base.IImage
import de.ism.database.base.IUri
import de.ism.database.keys.CreatorKey
import de.ism.database.keys.ProjectKey
import de.ism.database.keys.StructureKey
import de.ism.database.keys.TagKey
import de.ism.database.query.EProjectSorting
import de.ism.database.query.ESortingOrder
import de.ism.database.query.ISubsequenceIterator
import de.ism.database.views.IMaxProjectView
import de.ism.database.views.IMedProjectView
import de.ism.database.views.IMinProjectView
import java.time.LocalDate

/**
 * Represents the project table of a relational database.
 *
 * Created on 2020.12.29 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
@Suppress("TooManyFunctions")
interface IProjectTable : ITable {

    /**
     * Returns an iterator that provides the project keys of matching projects.
     */
    fun query(
        searchQuery: String,
        sorting: EProjectSorting,
        sortingOrder: ESortingOrder,
    ): ISubsequenceIterator<ProjectKey>

    /**
     * Returns a [IMinProjectView] from the given key.
     */
    fun getMinProject(key: ProjectKey): IMinProjectView

    /**
     * Returns a [IMedProjectView] from the given key.
     */
    fun getMedProject(key: ProjectKey): IMedProjectView

    /**
     * Returns a [IMaxProjectView] from the given key.
     */
    fun getMaxProject(key: ProjectKey): IMaxProjectView

    /**
     * Returns a Uri to the website of the [given project][key].
     */
    fun getSource(key: ProjectKey): IUri

    /**
     * Returns a title of the [given project][key].
     */
    fun getTitle(key: ProjectKey): String

    /**
     * Returns the creator key of the [given project][key].
     */
    fun getCreatorKey(key: ProjectKey): CreatorKey

    /**
     * Returns the keys of the structures of the [given project][key].
     */
    fun getStructureKeys(key: ProjectKey): Set<StructureKey>

    /**
     * Returns visual resources of the [given project][key].
     */
    fun getContent(key: ProjectKey): Set<IImage>

    /**
     * Returns tags associate to the [given project][key].
     */
    fun getTags(key: ProjectKey): Set<TagKey>

    /**
     * Returns the date when the [given project][key] was first published.
     */
    fun getPublicationDate(key: ProjectKey): LocalDate

    /**
     * Returns the date when the [given project][key] was last updated.
     */
    fun getUpgradeDate(key: ProjectKey): LocalDate?

    /**
     * Returns how often the ISM-intern project page was viewed.
     */
    fun getViews(key: ProjectKey): Int

    /**
     * Returns how often ISM users downloaded the [given project][key].
     */
    fun getDownloads(key: ProjectKey): Int

    /**
     * Returns how often ISM users added the [given project][key] to their favorites.
     */
    fun getFavorites(key: ProjectKey): Int
}
