package de.ism.database.importer

import com.github.doyaaaaaken.kotlincsv.dsl.context.CsvReaderContext
import de.ism.database.PathUtil
import java.net.MalformedURLException
import java.net.URI
import java.net.URL
import java.nio.file.Path

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

val importPath: Path = PathUtil.assetsPath.resolve("import")

val defaultReaderContext: CsvReaderContext.() -> Unit = {
    delimiter = ';'
}

fun String.toStringSet(): Set<String> {
    return this.removePrefix("[").removeSuffix("]").split(',').map {
        it.trim().removeSuffix(",").removePrefix("'").removeSuffix("'")
    }.toSet()
}

fun String.toUriSet(): Set<URI> {
    return this.toStringSet().mapNotNull { it.toNormalizedUriOrNull() }.toSet()
}

fun String.toNormalizedUri(): URI {
    val uri = URL(this.replace(" ", "%20"))
    return URL(uri.protocol, uri.host, uri.file.removeSuffix("/")).toURI()
}

fun String.toNormalizedUriOrNull(): URI? {
    return try {
        val uri = URL(this.replace(" ", "%20"))
        URL(uri.protocol, uri.host, uri.file.removeSuffix("/")).toURI()
    } catch (e: MalformedURLException) {
        null
    }
}
