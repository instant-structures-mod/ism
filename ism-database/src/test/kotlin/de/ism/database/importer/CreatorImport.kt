package de.ism.database.importer

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import de.ism.database.columns.CreatorSourceColumn
import de.ism.database.keys.CreatorKey
import java.net.URI

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

val creatorImport: Map<URI, CreatorImport> by lazy {
    val file = importPath.resolve("creators.csv").toFile()
    csvReader(defaultReaderContext).readAll(file).mapNotNull { row ->
        val entry = CreatorImport.fromList(row)

        val isValid = creatorCoCos.map {
            it.check(entry)
        }.all {
            it
        }

        if (isValid) {
            entry
        } else {
            null
        }
    }.associateBy { it.source }
}

val creatorCoCos: Set<ContextCondition<CreatorImport>> by lazy {
    buildSet {
        add(
            ContextCondition("name") {
                it.hasValidName()
            }
        )
        add(
            ContextCondition("Uri") {
                it.hasValidUri()
            }
        )
        add(
            ContextCondition("title") {
                it.hasValidRating()
            }
        )
    }
}

class CreatorImport(
    val name: String,
    val source: URI,
    val profileImage: URI?,
    val bannerImage: URI?,
    val rating: Double,
) {

    var key: CreatorKey = 0
    val projects: MutableSet<ProjectImport> = mutableSetOf()

    fun isValid(): Boolean {
        return hasValidName() && hasValidRating() && hasValidUri()
    }

    fun hasValidUri(): Boolean {
        return CreatorSourceColumn.creatorToUri(name) == source.toString()
            .removePrefix("https://www.planetminecraft.com/member/")
    }

    fun hasValidName(): Boolean {
        return name.isNotBlank()
    }

    fun hasValidRating(): Boolean {
        return rating >= 0.0
    }

    companion object {

        fun fromList(input: List<String>): CreatorImport {
            return CreatorImport(
                name = input[0],
                source = input[1].toNormalizedUriOrNull()!!,
                profileImage = input[2].toNormalizedUriOrNull(),
                bannerImage = input[3].toNormalizedUriOrNull(),
                rating = input[4].toDouble()
            )
        }
    }
}
