package de.ism.database.importer

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class ContextCondition<T>(
    val name: String,
    /**
     * returns if the given entry fulfills the condition.
     */
    val condition: (T) -> Boolean,
) {

    private val violations: MutableSet<T> = HashSet()

    fun check(entry: T): Boolean {
        return if (condition(entry)) {
            true
        } else {
            violations.add(entry)
            false
        }
    }

    override fun toString(): String {
        return "$name, violations: $violations"
    }
}
