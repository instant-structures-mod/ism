package de.ism.database.importer

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import de.ism.database.keys.StructureKey
import java.net.URI

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

val projectToStructure: Map<URI, URI> by lazy {
    val mapFile = importPath.resolve("schematic_url.csv").toFile()
    csvReader(defaultReaderContext).readAll(mapFile).mapNotNull { row ->
        val key = row[0].toNormalizedUriOrNull()
        val value = row[1].toNormalizedUriOrNull()

        if (key == null || value == null) {
            null
        } else {
            key to value
        }
    }.toMap()
}

val structureImport: Map<URI, StructureImport> by lazy {
    val file = importPath.resolve("structures.csv").toFile()
    csvReader(defaultReaderContext).readAll(file).mapNotNull { row ->
        val entry = StructureImport.fromList(row)

        val isValid = structureCoCos.map {
            it.check(entry)
        }.all {
            it
        }

        if (isValid) {
            entry
        } else {
            null
        }
    }.associateBy { it.projectSource }
}

val structureCoCos: Set<ContextCondition<StructureImport>> by lazy {
    buildSet {
        add(
            ContextCondition("blocks") {
                it.hasValidBlocks()
            }
        )
        add(
            ContextCondition("dimX") {
                it.hasValidDimX()
            }
        )
        add(
            ContextCondition("dimY") {
                it.hasValidDimY()
            }
        )
        add(
            ContextCondition("dimZ") {
                it.hasValidDimZ()
            }
        )
    }
}

class StructureImport(
    val name: String?,
    val projectSource: URI,
    val source: URI?,
    val blocks: Int,
    val dimX: Int,
    val dimY: Int,
    val dimZ: Int,
) {

    var key: StructureKey = 0

    fun isValid(): Boolean {
        return hasValidBlocks() && hasValidDimX() && hasValidDimY() && hasValidDimZ()
    }

    fun hasValidBlocks(): Boolean {
        return blocks in 1..1_999_999
    }

    fun hasValidDimX(): Boolean {
        return dimX in 1..256
    }

    fun hasValidDimY(): Boolean {
        return dimY in 1..256
    }

    fun hasValidDimZ(): Boolean {
        return dimZ in 1..256
    }

    companion object {

        fun fromList(input: List<String>): StructureImport {
            val projectSource = input[1].toNormalizedUri()
            return StructureImport(
                name = input[0],
                projectSource = projectSource,
                source = projectToStructure[projectSource],
                blocks = input[2].toInt(),
                dimX = input[3].toInt(),
                dimY = input[4].toInt(),
                dimZ = input[5].toInt(),
            )
        }
    }
}
