package de.ism.database.importer

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import de.ism.database.keys.ProjectKey
import java.net.URI
import java.time.LocalDateTime

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

val projectImport: Set<ProjectImport> by lazy {
    val path = importPath.resolve("projects.csv").toFile()
    csvReader(defaultReaderContext).readAll(path).mapNotNull { row ->
        val project = ProjectImport.fromList(row)

        val isValid = projectCoCos.map {
            it.check(project)
        }.all {
            it
        }

        if (isValid) {
            project
        } else {
            null
        }
    }.toSet()
}

val projectCoCos: Set<ContextCondition<ProjectImport>> by lazy {
    buildSet {
        add(
            ContextCondition("title") {
                it.hasValidTitle()
            }
        )
        add(
            ContextCondition("creator") {
                it.hasValidCreator()
            }
        )
        add(
            ContextCondition("structure") {
                it.hasValidStructure()
            }
        )
        add(
            ContextCondition("images") {
                it.hasValidImages()
            }
        )
        add(
            ContextCondition("tags") {
                it.hasValidTags()
            }
        )
        add(
            ContextCondition("views") {
                it.hasValidViews()
            }
        )
        add(
            ContextCondition("downloads") {
                it.hasValidDownloads()
            }
        )
        add(
            ContextCondition("favorites") {
                it.hasValidFavorites()
            }
        )
        add(
            ContextCondition("rating") {
                it.hasValidRating()
            }
        )
    }
}

@Suppress("LongParameterList")
class ProjectImport(
    val title: String,
    val creator: CreatorImport?,
    val structure: StructureImport?,
    val images: Set<URI>,
    val tags: Set<TagImport>,
    val source: URI,
    val publicationDate: LocalDateTime,
    val upgradeDate: LocalDateTime?,
    val views: Int,
    val downloads: Int,
    val favorites: Int,
    val rating: Double,
) {

    var key: ProjectKey = 0

    fun hasValidTitle(): Boolean {
        return title.isNotBlank()
    }

    fun hasValidCreator(): Boolean {
        return creator?.isValid() ?: false
    }

    fun hasValidStructure(): Boolean {
        return structure?.isValid() ?: false
    }

    fun hasValidImages(): Boolean {
        return images.isNotEmpty()
    }

    fun hasValidTags(): Boolean {
        return tags.isNotEmpty()
    }

    fun hasValidViews(): Boolean {
        return views >= 0
    }

    fun hasValidDownloads(): Boolean {
        return downloads >= 0
    }

    fun hasValidFavorites(): Boolean {
        return favorites >= 0
    }

    fun hasValidRating(): Boolean {
        return rating >= 0
    }

    companion object {

        fun fromList(input: List<String>): ProjectImport {
            val upgradeDate = if (input[6].isNotBlank()) {
                LocalDateTime.parse(input[6])
            } else {
                null
            }
            val source = input[4].toNormalizedUri()
            val tags = input[3].toStringSet().flatMap { raw ->
                raw.split(" ").map {
                    tagImport[it.lowercase()]
                }
            }.filterNotNull().toSet()

            return ProjectImport(
                title = input[0],
                creator = creatorImport[input[1].toNormalizedUri()],
                structure = structureImport[source],
                images = input[2].toUriSet(),
                tags = tags,
                source = source,
                publicationDate = LocalDateTime.parse(input[5]),
                upgradeDate = upgradeDate,
                views = input[7].toInt(),
                downloads = input[8].toInt(),
                favorites = input[9].toInt(),
                rating = input[10].toDouble(),
            )
        }
    }
}
