package de.ism.database.importer

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import de.ism.database.keys.TagKey

/**
 * <p>
 * created on 2022.04.25 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

/**
 * maps a tag-variant to their semantic meaning
 */
val tagImport: Map<String, TagImport> by lazy {
    val file = importPath.resolve("tags.csv").toFile()
    csvReader(defaultReaderContext).readAll(file).flatMap { row ->
        val entry = TagImport.fromList(row)
        entry.variants.map {
            it to entry
        }
    }.toMap()
}

@Suppress("UseDataClass")
class TagImport(
    val stem: String,
    val fullText: String,
    val variants: Set<String>,
) {

    var key: TagKey = 0

    companion object {

        fun fromList(input: List<String>): TagImport {
            return TagImport(
                stem = input[0],
                fullText = input[1],
                variants = input[2].toStringSet(),
            )
        }
    }
}
