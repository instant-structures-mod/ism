package de.ism.database.query

import org.assertj.core.api.Assertions
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test

/**
 * Created on 2021.01.06 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
class DescendingIteratorTest {

    @Test
    fun testInvalidInstantiation() {
        Assertions.assertThatThrownBy {
            DescendingIterator({ true }, { it }, 0)
        }.isInstanceOf(IllegalArgumentException::class.java)
    }

    @Test
    fun testReturnOrder() {
        val size = 100
        val source = IntArray(size) { it }
        val order = IntArray(size) { size - it - 1 }
        val touched = IntArray(size) { -1 }
        var index = 0

        val iterator = DescendingIterator(
            { true },
            {
                order[index] = source[it]
                touched[it] = touched[it] + 1
                index += 1
            },
            size
        )

        @Suppress("UnusedPrivateMember")
        for (i in 0..11) {
            iterator.next(subsequenceSize = 10)
        }

        SoftAssertions.assertSoftly { soft ->
            soft.assertThat(order).`as`("elements are returned from last to first")
                .isEqualTo(IntArray(size) { size - it - 1 })
            soft.assertThat(touched).`as`("elements are returned exactly once")
                .isEqualTo(IntArray(size) { 0 })
        }
    }

    @Test
    fun testReturnValid() {
        val valid = booleanArrayOf(true, false, true, true, false, false, true, false)

        val iterated: List<Int> = AscendingIterator(
            { valid[it] },
            { it },
            8
        ).next(10)

        Assertions.assertThat(iterated).contains(6, 3, 2, 0)
    }
}
