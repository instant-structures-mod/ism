package de.ism.database.query

import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Test

/**
 * Created on 2021.01.06 by Marc Schmidt
 *
 * GitLab: https://gitlab.com/SchmidtMarc
 *
 * License: Apache-2.0
 *
 * @author Marc Schmidt
 */
class EvaluatingIteratorTest {

    @Test
    fun testInvalidInstantiation() {
        SoftAssertions.assertSoftly {
            it.assertThatThrownBy {
                construct(0)
            }
            it.assertThatThrownBy {
                construct(Int.MIN_VALUE)
            }
        }
    }

    private fun construct(step: Int): EvaluatingIterator<Int> =
        EvaluatingIterator(
            evaluator = { true },
            producer = { it },
            firstIndex = 0,
            lastIndex = 10,
            step = step
        )
}
