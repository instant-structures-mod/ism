package de.ism.database.exporter

import de.ism.database.PathUtil
import org.assertj.core.api.Assertions
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.file.Path

/**
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class FixedSizeExporter<I, E, O>(
    val name: String,
) {

    open fun export(table: List<I>) {
        val input = input(table)
        validateInput(input)

        val adapted = adaptInput(input)
        validateAdapted(adapted)

        val transformed = transform(adapted)

        println("$name: transformed")

        write(transformed)

        println("$name: written")

        val actual = loadExported()
        val expected = toDestinationFormat(adapted)
        verify(actual, expected)

        println("$name: verified")
        println()
    }

    protected fun input(inputs: List<I>): List<E> {
        return inputs.map {
            input(it)
        }
    }

    abstract fun input(input: I): E

    protected fun validateInput(column: List<E>) {
        column.forEachIndexed { index, row ->
            validateInput(index, row)
        }
    }

    protected open fun validateInput(index: Int, row: E) {}

    protected fun adaptInput(column: List<E>): List<E> {
        return column.map {
            adaptInput(it)
        }
    }

    protected open fun adaptInput(row: E): E {
        return row
    }

    protected fun validateAdapted(column: List<E>) {
        column.forEachIndexed { index, row ->
            validateAdapted(index, row)
        }
    }

    protected open fun validateAdapted(index: Int, row: E) {}

    protected fun transform(column: List<E>): List<ByteArray> {
        return column.map { row ->
            transform(row)
        }
    }

    abstract fun transform(row: E): ByteArray

    protected fun write(arrays: List<ByteArray>) {
        FileOutputStream(exportPath().toFile()).use { out ->
            arrays.forEach { array ->
                write(array, out)
            }
        }
    }

    private fun exportPath(): Path {
        return PathUtil.assetsPath.resolve(name)
    }

    protected open fun write(array: ByteArray, output: OutputStream) {
        output.write(array)
    }

    abstract fun loadExported(): List<O>

    abstract fun toDestinationFormat(table: List<E>): List<O>

    protected open fun verify(actual: List<O>, expected: List<O>) {
        Assertions.assertThat(actual).hasSameSizeAs(expected)
        Assertions.assertThat(actual).hasSameElementsAs(expected)
    }
}
