package de.ism.database.exporter

import de.ism.database.importer.ProjectImport
import de.ism.database.tables.init.InitProjectTable
import org.assertj.core.api.Assertions

/**
 * <p>
 * created on 2022.05.01 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class TitleExport : HuffmanExporter<ProjectImport, String, String>("projects/title") {

    override fun input(input: ProjectImport): String {
        return input.title
    }

    override fun adaptInput(row: String): String {
        return row.replace(';', ',')
    }

    override fun validateAdapted(index: Int, row: String) {
        Assertions.assertThat(row).doesNotContain(";")
    }

    override fun transform(row: String): ByteArray {
        return row.toByteArray()
    }

    override fun loadExported(): List<String> {
        return InitProjectTable.loadTitles()
    }

    override fun toDestinationFormat(table: List<String>): List<String> {
        return table
    }
}
