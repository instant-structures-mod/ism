package de.ism.database.exporter

import de.ism.database.importer.ProjectImport
import java.time.LocalDate

/**
 * <p>
 * created on 2022.05.04 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class DateExporter(
    name: String,
) : FixedSizeExporter<ProjectImport, LocalDate?, LocalDate?>(name) {

    override fun validateInput(index: Int, row: LocalDate?) {
        if (row != null) {
            require(row.year >= 2000)
            require(row.year <= 2030)
        }
    }

    override fun transform(row: LocalDate?): ByteArray {
        return if (row == null) {
            byteArrayOf(0, 0)
        } else {
            val first = ((row.year - 2000).shl(1) or row.monthValue.shr(3)).toByte()
            val second = (row.monthValue.shl(5) or row.dayOfMonth).toByte()
            byteArrayOf(first, second)
        }
    }

    override fun toDestinationFormat(table: List<LocalDate?>): List<LocalDate?> {
        return table
    }
}
