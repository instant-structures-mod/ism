package de.ism.database.exporter

import com.github.doyaaaaaken.kotlincsv.dsl.context.CsvWriterContext
import com.github.doyaaaaaken.kotlincsv.dsl.context.WriteQuoteMode
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import de.ism.database.importer.*

/**
 * The open source variant of ISM contains only a portion of the potentially available structures.
 * We use these methods to restrict the import files, dependent on the available projects.
 * <p>
 * created on 2022.05.13 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
fun main() {
    RestrictImports.restrictStructures(Available.projects)
    RestrictImports.restrictTags(Available.projects)
    RestrictImports.restrictSchematicURLs(Available.projects)
}

object RestrictImports {

    private val defaultWriterContext: CsvWriterContext.() -> Unit = {
        delimiter = ';'
        nullCode = ""
        lineTerminator = System.lineSeparator()
        quote {
            mode = WriteQuoteMode.NON_NUMERIC
        }
    }

    internal fun restrictStructures(reachableProjects: List<ProjectImport>) {
        val projectURLs = reachableProjects.map {
            it.source
        }.toSet()

        val file = importPath.resolve("structures.csv").toFile()
        val rows = csvReader(defaultReaderContext).readAll(file).filter { row ->
            val entry = StructureImport.fromList(row)
            entry.projectSource in projectURLs
        }
        csvWriter(defaultWriterContext).writeAll(rows, file)
    }

    internal fun restrictSchematicURLs(reachableProjects: List<ProjectImport>) {
        val projectURLs = reachableProjects.map {
            it.source
        }.toSet()

        val file = importPath.resolve("schematic_url.csv").toFile()
        val rows = csvReader(defaultReaderContext).readAll(file).filter { row ->
            row[0].toNormalizedUri() in projectURLs
        }
        csvWriter(defaultWriterContext).writeAll(rows, file)
    }

    internal fun restrictTags(reachableProjects: List<ProjectImport>) {
        val tagStems = reachableProjects.flatMap { project ->
            project.tags.map {
                it.stem
            }
        }.toSet()

        val file = importPath.resolve("tags.csv").toFile()
        val rows = csvReader(defaultReaderContext).readAll(file).filter { row ->
            row[0] in tagStems
        }
        csvWriter(defaultWriterContext).writeAll(rows, file)
    }
}
