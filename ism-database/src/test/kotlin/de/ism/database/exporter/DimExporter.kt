package de.ism.database.exporter

import de.ism.database.importer.StructureImport

/**
 * <p>
 * created on 2022.05.04 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class DimExporter(
    name: String,
) : FixedSizeExporter<StructureImport, Int, Int>(name) {

    override fun validateInput(index: Int, row: Int) {
        require(row in 1..256)
    }

    override fun transform(row: Int): ByteArray {
        return byteArrayOf((row - 1).toByte())
    }

    override fun toDestinationFormat(table: List<Int>): List<Int> {
        return table
    }
}
