package de.ism.database.exporter

import de.ism.database.importer.ProjectImport
import de.ism.database.keys.StructureKey
import de.ism.database.tables.init.InitProjectTable
import org.assertj.core.api.Assertions

/**
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
class StructureKeyExport : FixedSizeExporter<ProjectImport, StructureKey, Set<StructureKey>>(
    "projects/structure_key"
) {

    private val uniqueKeys: MutableSet<StructureKey> = mutableSetOf()

    override fun input(input: ProjectImport): StructureKey {
        return input.structure!!.key
    }

    override fun validateInput(index: StructureKey, row: StructureKey) {
        Assertions.assertThat(uniqueKeys).doesNotContain(row)
        uniqueKeys.add(row)
    }

    override fun transform(row: StructureKey): ByteArray {
        val first = row.shr(8).toByte()
        val second = row.shr(0).toByte()
        return byteArrayOf(first, second)
    }

    override fun loadExported(): List<Set<StructureKey>> {
        return InitProjectTable.loadStructureKeys()
    }

    override fun toDestinationFormat(table: List<StructureKey>): List<Set<StructureKey>> {
        return table.map {
            mutableSetOf(it)
        }
    }
}
