package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.huffman.*
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import kotlin.math.round

/**
 * <p>
 * created on 2022.05.01 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class HuffmanExporter<I, E, O>(
    name: String,
) : FixedSizeExporter<I, E, O>(name) {

    override fun export(table: List<I>) {
        val input = input(table)
        validateInput(input)

        val adapted = adaptInput(input)
        validateAdapted(adapted)

        val transformed = transform(adapted)

        println("$name: transformed")

        val frequencies = frequencies(transformed)
        val frequencyTable = frequencyTable(frequencies)
        val codeTree = frequencyTable.buildCodeTree()
        val canonicalCode = CanonicalCode(codeTree, 257)

        val codeTable = codeTable(canonicalCode)
        Files.write(codeTreePath(), codeTable)

        val canonicalCodeTree = canonicalCode.toCodeTree()
        val encoded = encode(transformed, canonicalCodeTree)

        write(encoded)

        println("$name: encoded")

        val actual = loadExported()
        val expected = toDestinationFormat(adapted)
        verify(actual, expected)

        println("$name: verified")

        val histogram = histogram(transformed, encoded)
        val maxFactor = histogram.maxByOrNull { it.key }?.key ?: 1.0
        assignFactor(maxFactor)
        println("$name:\n${printHistogram(histogram)}")

        println()
    }

    // Huffman Encoding

    private fun frequencies(column: List<ByteArray>): IntArray {
        val frequencies = IntArray(257)
        column.forEach { row ->
            row.forEach {
                frequencies[it.toInt() and 0xFF] += 1
            }
        }
        return frequencies
    }

    private fun frequencyTable(frequencies: IntArray): FrequencyTable {
        val table = FrequencyTable(frequencies)
        table.increment(256) // EOF symbol gets a frequency of 1
        return table
    }

    private fun codeTable(canonCode: CanonicalCode): ByteArray {
        val out = ByteArrayOutputStream()
        val bitOut = BitOutputStream(out)
        HuffmanCompress.writeCodeLengthTable(bitOut, canonCode)
        return out.toByteArray()
    }

    private fun codeTreePath(): Path {
        val path = PathUtil.assetsPath.resolve("${name}_tree")
        Files.createDirectories(path.parent)
        return path
    }

    private fun encode(
        columns: List<ByteArray>,
        canonicalCodeTree: CodeTree,
    ): List<ByteArray> {
        return columns.map { row ->
            encode(row, canonicalCodeTree)
        }
    }

    open fun encode(
        row: ByteArray,
        canonicalCodeTree: CodeTree,
    ): ByteArray {
        val inputStream = ByteArrayInputStream(row)

        val outputStream = ByteArrayOutputStream()
        val bitOutputStream = BitOutputStream(outputStream)

        HuffmanCompress.compress(canonicalCodeTree, inputStream, bitOutputStream)
        bitOutputStream.close()

        return outputStream.toByteArray()
    }

    override fun write(array: ByteArray, output: OutputStream) {
        val sizeOne = array.size.shr(8).toByte().toInt()
        val sizeTwo = array.size.toByte().toInt()

        output.write(sizeOne)
        output.write(sizeTwo)
        output.write(array)
    }

    private fun histogram(
        transformed: List<ByteArray>,
        encoded: List<ByteArray>,
    ): MutableMap<Double, Int> {
        fun Double.round(decimals: Int): Double {
            var multiplier = 1.0
            repeat(decimals) { multiplier *= 10 }
            return round(this * multiplier) / multiplier
        }

        val map = mutableMapOf<Double, Int>()
        transformed.indices.forEach { index ->
            val transformedSize = transformed[index].size.toDouble()
            val encodedSize = encoded[index].size.toDouble()
            val ratio = (transformedSize / encodedSize).round(1)

            map[ratio] = map.getOrPut(ratio) { 0 } + 1
        }

        return map
    }

    abstract fun assignFactor(factor: Double)

    private fun printHistogram(map: Map<Double, Int>): String {
        return map.entries.sortedBy {
            it.key
        }.joinToString(separator = "\n") {
            "${it.key} -> ${it.value}"
        }
    }
}
