package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.columns.CreatorSourceColumn
import de.ism.database.exporter.Available.creators
import de.ism.database.importer.CreatorImport
import de.ism.database.tables.init.InitCreatorTable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import java.nio.file.Files

/**
 * <p>
 * created on 2022.04.27 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
object ExportCreators {

    fun export() {
        setup()

        exportNames()
        exportProjectKeys()

        tearDown()
    }

    @JvmStatic
    @BeforeAll
    fun setup() {
        Files.createDirectories(InitCreatorTable.path)

        InitCreatorTable.meta.size = creators.size

        println("creators: ${creators.size}")
    }

    @JvmStatic
    @AfterAll
    fun tearDown() {
        val json = Json {
            encodeDefaults = true
        }
        val encoded = json.encodeToString(InitCreatorTable.meta)
        val path = PathUtil.assetsPath.resolve("creators/meta.json")
        Files.write(path, encoded.toByteArray())
    }

    @Test
    @Order(1)
    fun exportNames() {
        object : HuffmanExporter<CreatorImport, String, String>("creators/names") {

            override fun input(input: CreatorImport): String {
                return input.name
            }

            override fun adaptInput(row: String): String {
                return row.replace("@", "")
            }

            override fun validateInput(index: Int, row: String) {
                val convertedName = CreatorSourceColumn.creatorToUri(row)
                val prefix = "https://www.planetminecraft.com/member/"
                val source = creators[index].source.toString()
                val constructedSource = source.removePrefix(prefix)

                Assertions.assertThat(convertedName).isEqualTo(constructedSource)
            }

            override fun transform(row: String): ByteArray {
                return row.toByteArray()
            }

            override fun assignFactor(factor: Double) {
                InitCreatorTable.meta.nameFactor = factor.toFloat()
            }

            override fun loadExported(): List<String> {
                return InitCreatorTable.loadNames()
            }

            override fun toDestinationFormat(table: List<String>): List<String> {
                return table
            }

            override fun verify(actual: List<String>, expected: List<String>) {
                super.verify(actual, expected)

                testCreatorSources()
            }

            /**
             * Design decision: We test the creator sources together with the creators' names
             * because they are stored together. Thereby, we can guarantee the sources are already
             * exported when the test case runs.
             */
            fun testCreatorSources() {
                val expected = creators.map {
                    it.source.toString()
                }
                val names = InitCreatorTable.loadNames()
                val actual = InitCreatorTable.loadSources(names).map {
                    it.getUri().toString()
                }

                Assertions.assertThat(actual).hasSameSizeAs(expected)
                Assertions.assertThat(actual).hasSameElementsAs(expected)
            }
        }.export(creators)
    }

    @Test
    fun exportProjectKeys() {
        object : KeySetExport<CreatorImport>("creators/project_keys") {

            override fun input(input: CreatorImport): Set<Int> {
                return input.projects.map { it.key }.toSet()
            }

            override fun loadExported(): List<Set<Int>> {
                return InitCreatorTable.loadProjects()
            }
        }.export(creators)
    }
}
