package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.exporter.Available.projects
import de.ism.database.exporter.Available.tags
import de.ism.database.importer.TagImport
import de.ism.database.keys.ProjectKey
import de.ism.database.tables.init.InitTagTable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.io.OutputStream
import java.nio.file.Files

/**
 * <p>
 * created on 2022.04.27 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

object ExportTags {

    const val marker: String = "."

    fun export() {
        setup()

        exportStemFullText()
        exportInvertedIndices()

        tearDown()
    }

    @JvmStatic
    @BeforeAll
    fun setup() {
        Files.createDirectories(InitTagTable.path)

        InitTagTable.meta.size = tags.size
        InitTagTable.meta.markerCode = marker.first().code

        println("tags: ${tags.size}")
    }

    @JvmStatic
    @AfterAll
    fun tearDown() {
        val json = Json {
            encodeDefaults = true
        }
        val encoded = json.encodeToString(InitTagTable.meta)
        val path = PathUtil.assetsPath.resolve("tags/meta.json")
        Files.write(path, encoded.toByteArray())
    }

    @Test
    fun exportStemFullText() {
        object : HuffmanExporter<TagImport, String, Pair<String, String>>("tags/tags") {

            override fun input(input: TagImport): String {
                Assertions.assertThat(input.fullText).doesNotContain(marker)
                Assertions.assertThat(input.stem).doesNotContain(marker)

                return if (input.fullText == input.stem) {
                    input.stem
                } else if (input.fullText.startsWith(input.stem)) {
                    val suffix = input.fullText.removePrefix(input.stem)
                    input.stem + marker + suffix
                } else {
                    input.stem + marker + marker + input.fullText
                }
            }

            override fun write(array: ByteArray, output: OutputStream) {
                Assertions.assertThat(array).hasSizeLessThanOrEqualTo(256)

                output.write(array.size)
                output.write(array)
            }

            override fun assignFactor(factor: Double) {
                InitTagTable.meta.tagFactor = factor.toFloat()
            }

            override fun transform(row: String): ByteArray {
                return row.toByteArray()
            }

            override fun loadExported(): List<Pair<String, String>> {
                val (stems, fullTexts) = InitTagTable.loadTags()
                return stems.zip(fullTexts)
            }

            override fun toDestinationFormat(table: List<String>): List<Pair<String, String>> {
                return tags.map {
                    it.stem to it.fullText
                }
            }
        }.export(tags)
    }

    @Test
    fun exportInvertedIndices() {
        val stemToProjects = mutableMapOf<String, MutableSet<ProjectKey>>()
        projects.forEach { project ->
            project.tags.forEach { tag ->
                stemToProjects.getOrPut(tag.stem) {
                    mutableSetOf()
                }.add(project.key)
            }
        }

        object : FixedSizeExporter<TagImport, Set<ProjectKey>, Set<ProjectKey>>(
            "tags/project_keys"
        ) {

            override fun input(input: TagImport): Set<ProjectKey> {
                return stemToProjects[input.stem]!!
            }

            override fun transform(row: Set<ProjectKey>): ByteArray {
                val bytes = ByteArray(2 * row.size)
                row.forEachIndexed { index, key ->
                    bytes[2 * index + 0] = key.shr(8).toByte()
                    bytes[2 * index + 1] = key.toByte()
                }
                return bytes
            }

            override fun write(array: ByteArray, output: OutputStream) {
                val sizeOne = array.size.shr(8).toByte().toInt()
                val sizeTwo = array.size.toByte().toInt()

                output.write(sizeOne)
                output.write(sizeTwo)
                output.write(array)
            }

            override fun loadExported(): List<Set<ProjectKey>> {
                return InitTagTable.loadProjectKeys()
            }

            override fun toDestinationFormat(table: List<Set<ProjectKey>>): List<Set<ProjectKey>> {
                return table
            }
        }.export(tags)
    }
}
