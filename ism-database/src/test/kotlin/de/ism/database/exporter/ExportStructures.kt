package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.base.IUri
import de.ism.database.base.Uri
import de.ism.database.columns.StructureSourceColumn
import de.ism.database.importer.StructureImport
import de.ism.database.tables.init.InitStructureTable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.nio.file.Files

/**
 * <p>
 * created on 2022.04.27 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

object ExportStructures {

    fun export() {
        setup()

        exportSource()
        exportBlocks()
        exportDimX()
        exportDimY()
        exportDimZ()

        tearDown()
    }

    @JvmStatic
    @BeforeAll
    fun setup() {
        Files.createDirectories(InitStructureTable.path)

        InitStructureTable.meta.size = Available.structures.size

        println("structures: ${Available.structures.size}")
    }

    @JvmStatic
    @AfterAll
    fun tearDown() {
        val json = Json {
            encodeDefaults = true
        }
        val encoded = json.encodeToString(InitStructureTable.meta)
        val path = PathUtil.assetsPath.resolve("structures/meta.json")
        Files.write(path, encoded.toByteArray())
    }

    @Test
    fun exportSource() {
        object : SourceExport<StructureImport, IUri>(
            name = "structures/source",
            prefix = StructureSourceColumn.PREFIX,
            suffix = StructureSourceColumn.SUFFIX
        ) {

            override fun input(input: StructureImport): String {
                return input.source!!.toString()
            }

            override fun assignFactor(factor: Double) {
                InitStructureTable.meta.sourceFactor = factor.toFloat()
            }

            override fun loadExported(): List<IUri> {
                return InitStructureTable.loadSource()
            }

            override fun toDestinationFormat(table: List<String>): List<IUri> {
                return table.map {
                    Uri(prefix + it + suffix)
                }
            }
        }.export(Available.structures)
    }

    @Test
    fun exportBlocks() {
        object : FixedSizeExporter<StructureImport, Int, Int>(
            name = "structures/blocks"
        ) {

            override fun input(input: StructureImport): Int {
                return input.blocks
            }

            override fun transform(row: Int): ByteArray {
                return byteArrayOf(row.shr(16).toByte(), row.shr(8).toByte(), row.toByte())
            }

            override fun loadExported(): List<Int> {
                return InitStructureTable.loadBlocks()
            }

            override fun toDestinationFormat(table: List<Int>): List<Int> {
                return table
            }
        }.export(Available.structures)
    }

    @Test
    fun exportDimX() {
        object : DimExporter("structures/dim_x") {

            override fun input(input: StructureImport): Int {
                return input.dimX
            }

            override fun loadExported(): List<Int> {
                return InitStructureTable.loadDimX()
            }
        }.export(Available.structures)
    }

    @Test
    fun exportDimY() {
        object : DimExporter("structures/dim_y") {

            override fun input(input: StructureImport): Int {
                return input.dimY
            }

            override fun loadExported(): List<Int> {
                return InitStructureTable.loadDimY()
            }
        }.export(Available.structures)
    }

    @Test
    fun exportDimZ() {
        object : DimExporter("structures/dim_z") {

            override fun input(input: StructureImport): Int {
                return input.dimZ
            }

            override fun loadExported(): List<Int> {
                return InitStructureTable.loadDimZ()
            }
        }.export(Available.structures)
    }
}
