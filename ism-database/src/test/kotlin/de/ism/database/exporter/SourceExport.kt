package de.ism.database.exporter

import org.assertj.core.api.Assertions

/**
 * <p>
 * created on 2022.05.01 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class SourceExport<I, O>(
    name: String,
    protected val prefix: String = "",
    protected val suffix: String = "",
) : HuffmanExporter<I, String, O>(name) {

    override fun adaptInput(row: String): String {
        return row.removePrefix(prefix).removeSuffix(suffix)
    }

    override fun validateInput(index: Int, row: String) {
        Assertions.assertThat(row).startsWith(prefix)
        Assertions.assertThat(row).endsWith(suffix)
    }

    override fun validateAdapted(index: Int, row: String) {
        Assertions.assertThat(row).doesNotContain(";")
    }

    override fun transform(row: String): ByteArray {
        return row.toByteArray()
    }
}
