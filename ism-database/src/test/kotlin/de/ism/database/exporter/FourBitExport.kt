package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.importer.ProjectImport
import org.assertj.core.api.Assertions
import java.io.FileOutputStream
import java.nio.file.Path
import kotlin.math.abs

/**
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class FourBitExport(
    private val map: IntArray,
    private val name: String,
) {

    private val histogram: MutableMap<Int, Int> = mutableMapOf()

    fun export(projects: List<ProjectImport>) {
        val input = input(projects)

        val adapted = input.map { adaptInput(it) }

        histogram(adapted)
        print(printHistogram())

        write(adapted)

        println("$name: written")

        val exported = loadExported()

        val expected = adapted.map {
            map[it]
        }
        verify(exported, expected)

        println("$name: verified")
        println()
    }

    protected fun input(inputs: List<ProjectImport>): List<Int> {
        return inputs.map {
            input(it)
        }
    }

    abstract fun input(input: ProjectImport): Int

    open fun adaptInput(row: Int): Int {
        map.forEachIndexed { index, value ->
            if (row <= value) {
                return index
            }
        }
        return 15
    }

    private fun histogram(entries: List<Int>) {
        entries.forEach {
            histogram[it] = histogram.getOrPut(it) {
                0
            } + 1
        }
    }

    private fun printHistogram(): String {
        return "$name:\n" + histogram.entries.sortedBy {
            it.key
        }.joinToString(separator = "\n") {
            "${it.key}. ${map[it.key]} -> ${it.value}"
        } + "\nsize: ${histogram.size}"
    }

    fun write(entries: List<Int>) {
        val even = if (entries.size % 2 == 0) {
            entries
        } else {
            entries + 0
        }

        FileOutputStream(exportPath().toFile()).use { out ->
            val size = (entries.size / 2.0).toInt()

            for (index in 0 until size) {
                val first = even[2 * index + 0]
                val second = even[2 * index + 1]
                val combined = combine(first, second)
                out.write(combined)
            }
        }
    }

    private fun combine(first: Int, second: Int): Int {
        return first.shl(4) or second
    }

    private fun exportPath(): Path {
        return PathUtil.assetsPath.resolve(name)
    }

    abstract fun loadExported(): List<Int>

    private fun verify(actual: List<Int>, expected: List<Int>) {
        Assertions.assertThat(abs(actual.size - expected.size)).isLessThan(2)
        Assertions.assertThat(actual).hasSameElementsAs(actual)
    }
}
