package de.ism.database.exporter

import de.ism.database.importer.*
import org.junit.jupiter.api.Test

/**
 * <p>
 * created on 2022.04.26 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

@Test
fun main() {
    ExportProjects.export()
    ExportCreators.export()
    ExportStructures.export()
    ExportTags.export()
}

object Available {

    val projects: List<ProjectImport> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        _projects
        creators
        structures
        tags
        _projects
    }

    private val _projects: List<ProjectImport> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val reachableProjects = projectImport.sortedByDescending {
            it.rating
        }
        reachableProjects.forEachIndexed { index, project ->
            project.key = index
            project.creator!!.projects.add(project)
        }
        reachableProjects
    }

    val creators: List<CreatorImport> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val reachableCreators = _projects.mapNotNull {
            it.creator
        }.toSet().sortedByDescending {
            it.rating
        }
        reachableCreators.forEachIndexed { index, creator ->
            creator.key = index
        }
        reachableCreators
    }

    val structures: List<StructureImport> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val reachableStructures = _projects.mapNotNull {
            it.structure
        }.toSet().toList()
        reachableStructures.forEachIndexed { index, structure ->
            structure.key = index
        }
        reachableStructures
    }

    val tags: List<TagImport> by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val reachableTags = _projects.flatMap {
            it.tags
        }.toSet().sortedBy {
            it.stem
        }
        reachableTags.forEachIndexed { index, tag ->
            tag.key = index
        }
        reachableTags
    }
}
