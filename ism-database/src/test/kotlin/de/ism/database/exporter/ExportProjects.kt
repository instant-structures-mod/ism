package de.ism.database.exporter

import de.ism.database.PathUtil
import de.ism.database.base.IUri
import de.ism.database.base.Uri
import de.ism.database.columns.ProjectSourceColumn
import de.ism.database.importer.ProjectImport
import de.ism.database.keys.CreatorKey
import de.ism.database.tables.init.InitProjectTable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.nio.file.Files
import java.time.LocalDate

/**
 * <p>
 * created on 2022.04.27 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */

object ExportProjects {

    fun export() {
        ExportStructures.setup()

        exportSource()
        exportImages()
        exportTitle()
        exportCreatorKey()
        exportStructureKey()
        exportTagKeys()
        exportPublicationDate()
        exportUpgradeDate()
        exportViews()
        exportDownloads()
        exportFavorites()

        tearDown()
    }

    @JvmStatic
    @BeforeAll
    fun setup() {
        Files.createDirectories(InitProjectTable.path)

        InitProjectTable.meta.size = Available.projects.size

        println("projects: ${Available.projects.size}")
    }

    @JvmStatic
    @AfterAll
    fun tearDown() {
        val json = Json {
            encodeDefaults = true
        }
        val encoded = json.encodeToString(InitProjectTable.meta)
        val path = PathUtil.assetsPath.resolve("projects/meta.json")
        Files.write(path, encoded.toByteArray())
    }

    @Test
    fun exportStructureKey() {
        StructureKeyExport().export(Available.projects)
    }

    @Test
    fun exportCreatorKey() {
        object : FixedSizeExporter<ProjectImport, CreatorKey, CreatorKey>(
            "projects/creator_key"
        ) {

            override fun input(input: ProjectImport): CreatorKey {
                return input.creator!!.key
            }

            override fun transform(row: CreatorKey): ByteArray {
                val first = row.shr(8).toByte()
                val second = row.toByte()
                return byteArrayOf(first, second)
            }

            override fun loadExported(): List<CreatorKey> {
                return InitProjectTable.loadCreatorKeys()
            }

            override fun toDestinationFormat(table: List<CreatorKey>): List<CreatorKey> {
                return table
            }
        }.export(Available.projects)
    }

    @Test
    fun exportTitle() {
        object : TitleExport() {

            override fun assignFactor(factor: Double) {
                InitProjectTable.meta.titleFactor = factor.toFloat()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportImages() {
        object : ImagesExport() {

            override fun assignFactor(factor: Double) {
                InitProjectTable.meta.imagesFactor = factor.toFloat()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportSource() {
        object : SourceExport<ProjectImport, IUri>("projects/source", ProjectSourceColumn.PREFIX) {

            override fun input(input: ProjectImport): String {
                return input.source.toString()
            }

            override fun assignFactor(factor: Double) {
                InitProjectTable.meta.sourceFactor = factor.toFloat()
            }

            override fun loadExported(): List<IUri> {
                return InitProjectTable.loadSource()
            }

            override fun toDestinationFormat(table: List<String>): List<IUri> {
                return table.map {
                    Uri(prefix + it + suffix)
                }
            }
        }.export(Available.projects)
    }

    @Test
    fun exportTagKeys() {
        object : KeySetExport<ProjectImport>("projects/tag_keys") {

            override fun input(input: ProjectImport): Set<Int> {
                return input.tags.map { it.key }.toSet()
            }

            override fun loadExported(): List<Set<Int>> {
                return InitProjectTable.loadTagKeys()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportPublicationDate() {
        object : DateExporter("projects/publication_date") {

            override fun input(input: ProjectImport): LocalDate? {
                return input.publicationDate.toLocalDate()
            }

            override fun loadExported(): List<LocalDate?> {
                return InitProjectTable.loadPublicationDate()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportUpgradeDate() {
        object : DateExporter("projects/upgrade_date") {

            override fun input(input: ProjectImport): LocalDate? {
                return input.upgradeDate?.toLocalDate()
            }

            override fun loadExported(): List<LocalDate?> {
                return InitProjectTable.loadUpgradeDate()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportViews() {
        InitProjectTable.meta.viewsMap = intArrayOf(
            100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1250, 1500, 2000, 2500, 5000, 7500,
        )

        object : FourBitExport(InitProjectTable.meta.viewsMap, "projects/views") {

            override fun input(input: ProjectImport): Int {
                return input.views
            }

            override fun loadExported(): List<Int> {
                return InitProjectTable.loadViews()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportDownloads() {
        InitProjectTable.meta.downloadsMap = intArrayOf(
            5, 10, 25, 50, 75, 100, 150, 200, 300, 400, 500, 750, 1000, 1500, 2000, 2500,
        )

        object : FourBitExport(InitProjectTable.meta.downloadsMap, "projects/downloads") {

            override fun input(input: ProjectImport): Int {
                return input.downloads
            }

            override fun loadExported(): List<Int> {
                return InitProjectTable.loadDownloads()
            }
        }.export(Available.projects)
    }

    @Test
    fun exportFavorites() {
        InitProjectTable.meta.favoritesMap = intArrayOf(
            0, 1, 2, 3, 4, 5, 10, 15, 20, 25, 50, 60, 70, 80, 90, 100,
        )

        object : FourBitExport(InitProjectTable.meta.favoritesMap, "projects/favorites") {

            override fun input(input: ProjectImport): Int {
                return input.favorites
            }

            override fun loadExported(): List<Int> {
                return InitProjectTable.loadFavorites()
            }
        }.export(Available.projects)
    }
}
