package de.ism.database.exporter

import org.assertj.core.api.Assertions
import java.io.OutputStream

/**
 * <p>
 * created on 2022.05.02 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class KeySetExport<I>(
    name: String,
) : FixedSizeExporter<I, Set<Int>, Set<Int>>(name) {

    override fun validateInput(index: Int, row: Set<Int>) {
        row.forEach {
            Assertions.assertThat(it).isLessThanOrEqualTo(Char.MAX_VALUE.code)
        }

        Assertions.assertThat(row.size).isLessThanOrEqualTo(UByte.MAX_VALUE.toInt())
    }

    override fun transform(row: Set<Int>): ByteArray {
        val array = ByteArray(2 * row.size)
        row.forEachIndexed { index, tag ->
            array[2 * index + 0] = tag.shr(8).toByte()
            array[2 * index + 1] = tag.toByte()
        }
        return array
    }

    override fun write(array: ByteArray, output: OutputStream) {
        val size = array.size / 2
        output.write(size)
        output.write(array)
    }

    override fun toDestinationFormat(table: List<Set<Int>>): List<Set<Int>> {
        return table
    }
}
