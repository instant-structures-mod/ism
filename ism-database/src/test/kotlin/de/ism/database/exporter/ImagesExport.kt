package de.ism.database.exporter

import de.ism.database.base.IImage
import de.ism.database.base.Image
import de.ism.database.columns.ImagesColumn.Companion.COMPRESSION_MARKER
import de.ism.database.columns.ImagesColumn.Companion.PREFIX_ONE
import de.ism.database.columns.ImagesColumn.Companion.PREFIX_ONE_MARKER
import de.ism.database.columns.ImagesColumn.Companion.PREFIX_TWO
import de.ism.database.columns.ImagesColumn.Companion.PREFIX_TWO_MARKER
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_ONE
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_ONE_MARKER
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_THREE
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_THREE_MARKER
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_TWO
import de.ism.database.columns.ImagesColumn.Companion.SUFFIX_TWO_MARKER
import de.ism.database.huffman.BitOutputStream
import de.ism.database.huffman.CodeTree
import de.ism.database.huffman.HuffmanCompress
import de.ism.database.importer.ProjectImport
import de.ism.database.tables.init.InitProjectTable
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream

/**
 * <p>
 * created on 2022.04.30 by Marc Schmidt
 * <p>
 * mail: MaggiCraftInfo@gmail.com
 * <p>
 * gitlab: https://gitlab.com/SchmidtMarc
 *
 * @author Marc Schmidt
 */
abstract class ImagesExport :
    HuffmanExporter<ProjectImport, Set<String>, Set<IImage>>("projects/images") {

    // general

    override fun input(input: ProjectImport): Set<String> {
        return input.images.map {
            it.toString()
        }.toSet()
    }

    override fun adaptInput(row: Set<String>): Set<String> {
        return row.filter {
            matchingSuffixesPrefixes(it)
        }.toSet()
    }

    override fun validateAdapted(index: Int, row: Set<String>) {
        requireUniformPrefixes(index, row)

        row.forEach { entry ->
            require(entry.startsWith(PREFIX_ONE) || entry.startsWith(PREFIX_TWO)) {
                "$entry of row $index has no prefix"
            }
            require(';' !in entry) {
                "$entry of row $index contains ';'"
            }
            require(COMPRESSION_MARKER.toInt().toChar() !in entry)
            require(SUFFIX_ONE_MARKER.toInt().toChar() !in entry) {
                "$entry of row $index contains '${SUFFIX_ONE_MARKER.toInt().toChar()}'"
            }
            require(SUFFIX_TWO_MARKER.toInt().toChar() !in entry) {
                "$entry of row $index contains '${SUFFIX_TWO_MARKER.toInt().toChar()}'"
            }
            require(SUFFIX_THREE_MARKER.toInt().toChar() !in entry) {
                "$entry of row $index contains '${SUFFIX_THREE_MARKER.toInt().toChar()}'"
            }
        }
    }

    override fun transform(row: Set<String>): ByteArray {
        return if (row.isEmpty()) {
            byteArrayOf(PREFIX_ONE_MARKER)
        } else {
            val first = row.first().toString()
            val prefix = prefix(first)

            val transformed = row.map {
                transform(it)
            }
            val maxLength = 2 * maxLength(transformed)
            var payload = transformed.joinToString(separator = "")

            val bestFit = bestFit(payload, maxLength)
            val replaced =
                payload.replace(bestFit.text, COMPRESSION_MARKER.toInt().toChar().toString())
            payload = "${bestFit.text}${COMPRESSION_MARKER.toInt().toChar()}$replaced"

            "$prefix$payload;".toByteArray()
        }
    }

    private fun maxLength(texts: Iterable<String>): Int {
        return texts.maxOf { it.length }
    }

    private fun bestFit(text: String, maxLength: Int): BestFit {
        val subToQuan = HashMap<String, Int>()

        for (i in text.indices) {
            for (j in i + 2..Integer.min(i + maxLength, text.lastIndex)) {
                val substring = text.substring(i, j)
                subToQuan[substring] = subToQuan.getOrDefault(substring, 0) + 1
            }
        }

        return subToQuan.toList().map { pair ->
            BestFit(pair.first, pair.second)
        }.maxByOrNull {
            it.value
        }!!
    }

    private fun transform(entry: String): String {
        val withoutPrefix = entry.removePrefix(PREFIX_ONE).removePrefix(PREFIX_TWO)

        require(withoutPrefix.count { it == '.' } == 1) { withoutPrefix }

        return suffixMarked(withoutPrefix)
    }

    // custom

    private fun matchingSuffixesPrefixes(text: String): Boolean {
        return (text.startsWith(PREFIX_ONE) || text.startsWith(PREFIX_TWO)) &&
            (text.endsWith(SUFFIX_ONE) || text.endsWith(SUFFIX_TWO) || text.endsWith(SUFFIX_THREE))
    }

    private fun prefix(image: String): Char {
        return if (image.startsWith(PREFIX_ONE)) {
            PREFIX_ONE_MARKER.toInt().toChar()
        } else {
            PREFIX_TWO_MARKER.toInt().toChar()
        }
    }

    private fun suffixMarked(text: String): String {
        return text
            .replace(SUFFIX_ONE, SUFFIX_ONE_MARKER.toInt().toChar().toString())
            .replace(SUFFIX_TWO, SUFFIX_TWO_MARKER.toInt().toChar().toString())
            .replace(SUFFIX_THREE, SUFFIX_THREE_MARKER.toInt().toChar().toString())
    }

    /**
     * The images a one project have all the same prefixes.
     */
    private fun requireUniformPrefixes(index: Int, row: Set<String>) {
        val hasPrefixOne = row.any {
            it.startsWith(PREFIX_ONE)
        }
        val hasPrefixTwo = row.any {
            it.startsWith(PREFIX_TWO)
        }
        val hasMixedPrefixes = hasPrefixOne && hasPrefixTwo

        require(!hasMixedPrefixes) {
            "images of project $index have different prefixes"
        }
    }

    override fun encode(row: ByteArray, canonicalCodeTree: CodeTree): ByteArray {
        val input = row.drop(1).toByteArray()
        val inputStream = ByteArrayInputStream(input)

        val outputStream = ByteArrayOutputStream()
        val bitOutputStream = BitOutputStream(outputStream)

        bitOutputStream.write(row.first().toInt())
        HuffmanCompress.compress(canonicalCodeTree, inputStream, bitOutputStream)
        bitOutputStream.close()

        return outputStream.toByteArray()
    }

    // verification

    override fun loadExported(): List<Set<IImage>> {
        return InitProjectTable.loadImages()
    }

    override fun toDestinationFormat(table: List<Set<String>>): List<Set<IImage>> {
        return table.map { row ->
            row.map { entry ->
                Image(entry)
            }.toSet()
        }
    }
}

data class BestFit(
    val text: String,
    val quantity: Int,
    val value: Int = text.length * (quantity - 1),
    val length: Int = text.length,
)
