package de.ism.database.huffman

import de.ism.database.huffman.HuffmanCompress.compress
import de.ism.database.huffman.HuffmanCompress.writeCodeLengthTable
import de.ism.database.tables.init.InitTables
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.OutputStream

/*
 * Reference Huffman coding
 * Copyright (c) Project Nayuki
 *
 * https://www.nayuki.io/page/reference-huffman-coding
 * https://github.com/nayuki/Reference-Huffman-coding
 */

/**
 * Tests [HuffmanCompress] coupled with [HuffmanDecompress].
 */
class HuffmanCompressTest : HuffmanCodingTest() {

    @Throws(IOException::class)
    override fun compress(b: ByteArray): ByteArray {
        val frequencies = FrequencyTable(IntArray(257))
        for (x in b) frequencies.increment(x.toInt() and 0xFF)
        frequencies.increment(256) // EOF symbol gets a frequency of 1
        var code = frequencies.buildCodeTree()
        val canonCode = CanonicalCode(code, 257)
        code = canonCode.toCodeTree()
        val input = ByteArrayInputStream(b)
        val out = ByteArrayOutputStream()
        val bitOut = BitOutputStream(out)
        writeCodeLengthTable(bitOut, canonCode)
        compress(code, input, bitOut)
        bitOut.close()
        return out.toByteArray()
    }

    @Throws(IOException::class)
    override fun decompress(b: ByteArray?): ByteArray {
        val input = ByteArrayInputStream(b)
        val out = ByteArrayOutputStream()
        val bitIn = BitInputStream(input)
        val canonCode = InitTables.readCodeLengthTable(bitIn)
        val code = canonCode.toCodeTree()
        decompress(code, bitIn, out)
        return out.toByteArray()
    }

    // To allow unit testing, this method is package-private instead of private.
    @Throws(IOException::class)
    fun decompress(code: CodeTree, input: BitInputStream, out: OutputStream) {
        while (true) {
            val symbol = HuffmanDecompress.read(code, input)
            // EOF symbol
            if (symbol == 256) {
                break
            }
            out.write(symbol)
        }
    }
}
// raw string: 745,81 + 731,19 + 719,81 + 824,06 = 3,02mb
// byte array: 2,01mb
// byte array: 1,77mb + 26,78kb

// 341,43

// 4,41
// 4,88
// 8,17
// 9,69 = 2,83 + 2,5 + 2,21 + 2,15
