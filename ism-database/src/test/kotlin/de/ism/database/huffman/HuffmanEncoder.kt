package de.ism.database.huffman

import java.io.IOException

/*
 * Reference Huffman coding
 * Copyright (c) Project Nayuki
 *
 * https://www.nayuki.io/page/reference-huffman-coding
 * https://github.com/nayuki/Reference-Huffman-coding
 */

/**
 * Encodes symbols and writes to a Huffman-coded bit stream. Not thread-safe.
 *
 * Constructs a Huffman encoder based on the specified bit output stream.
 */
class HuffmanEncoder(
    /**
     * The underlying bit output stream (not null).
     */
    private val output: BitOutputStream,
    /**
     * The code tree to use in the next {@link#write(int)} operation. Must be given a non-`null`
     * value before calling write(). The tree can be changed after each symbol encoded, as long
     * as the encoder and decoder have the same tree at the same point in the code stream.
     */
    var codeTree: CodeTree
) {
    /**
     * Encodes the specified symbol and writes to the Huffman-coded output stream.
     * @param symbol the symbol to encode, which is non-negative and must be in the range of the
     * code tree
     * @throws IOException if an I/O exception occurred
     * @throws NullPointerException if the current code tree is `null`
     * @throws IllegalArgumentException if the symbol value is negative or has no binary code
     */
    @Throws(IOException::class)
    fun write(symbol: Int) {
        val bits = codeTree.getCode(symbol)
        for (b in bits!!) {
            output.write(b)
        }
    }
}
