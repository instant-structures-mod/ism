package de.ism.database.huffman

import java.io.IOException
import java.io.InputStream

/*
 * Reference Huffman coding
 * Copyright (c) Project Nayuki
 *
 * https://www.nayuki.io/page/reference-huffman-coding
 * https://github.com/nayuki/Reference-Huffman-coding
 */ /**
 * Compression application using static Huffman coding.
 *
 * Usage: java HuffmanCompress InputFile OutputFile
 *
 * Then use the corresponding "HuffmanDecompress" application to recreate the original input file.
 *
 * Note that the application uses an alphabet of 257 symbols - 256 symbols for the byte values
 * and 1 symbol for the EOF marker. The compressed file format starts with a list of 257
 * code lengths, treated as a canonical code, and then followed by the Huffman-coded data.
 */
object HuffmanCompress {

    // To allow unit testing, this method is package-private instead of private.
    @Throws(IOException::class)
    fun writeCodeLengthTable(out: BitOutputStream, canonCode: CanonicalCode) {
        for (i in 0 until canonCode.getSymbolLimit()) {
            val value = canonCode.getCodeLength(i)
            // For this file format, we only support codes up to 255 bits long
            require(value < 256) {
                "The code for a symbol is too long"
            }

            // Write value as 8 bits in big endian
            for (j in 7 downTo 0) {
                out.write(value ushr j and 1)
            }
        }
    }

    // To allow unit testing, this method is package-private instead of private.
    @Throws(IOException::class)
    fun compress(code: CodeTree, input: InputStream, out: BitOutputStream) {
        val enc = HuffmanEncoder(out, code)
        while (true) {
            val byte = input.read()
            if (byte == -1) {
                break
            }
            enc.write(byte)
        }
        enc.write(256) // EOF
    }
}
