package de.ism.database.huffman

/*
 * Reference Huffman coding
 * Copyright (c) Project Nayuki
 *
 * https://www.nayuki.io/page/reference-huffman-coding
 * https://github.com/nayuki/Reference-Huffman-coding
 */

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.EOFException
import java.io.IOException
import java.util.*

/**
 * Tests the compression and decompression of a complete Huffman coding application, using the
 * JUnit test framework.
 */
abstract class HuffmanCodingTest {

    /* Test cases */
    @Test
    fun testEmpty() {
        test(ByteArray(0))
    }

    @Test
    fun testOneSymbol() {
        test(ByteArray(10))
    }

    @Test
    fun testSimple() {
        test(byteArrayOf(0, 3, 1, 2))
    }

    @Test
    fun testEveryByteValue() {
        val array = ByteArray(256)
        for (i in array.indices) array[i] = i.toByte()
        test(array)
    }

    @Test
    fun testRandomShort() {
        for (i in 0..99) {
            val array = ByteArray(random.nextInt(1000))
            random.nextBytes(array)
            test(array)
        }
    }

    @Test
    fun testRandomLong() {
        for (i in 0..2) {
            val array = ByteArray(random.nextInt(1000000))
            random.nextBytes(array)
            test(array)
        }
    }

    /* Utilities */ // Tests that the given byte array can be compressed and decompressed to the
    // same data, and not throw any exceptions.
    private fun test(b: ByteArray) {
        try {
            val compressed = compress(b)
            val decompressed = decompress(compressed)
            Assertions.assertArrayEquals(b, decompressed)
        } catch (e: EOFException) {
            Assertions.fail("Unexpected EOF", e)
        } catch (e: IOException) {
            throw AssertionError(e)
        }
    }

    /* Abstract methods */ // Compression method that needs to be supplied by a subclass.
    @Throws(IOException::class)
    protected abstract fun compress(b: ByteArray): ByteArray

    // Decompression method that needs to be supplied by a subclass.
    @Throws(IOException::class)
    protected abstract fun decompress(b: ByteArray?): ByteArray

    companion object {
        private val random = Random()
    }
}
