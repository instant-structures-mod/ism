# Instant Structures Mod (ISM)

![Instant Structures Mod, title image](assets/images/title.png)

This repository contains the open-source project for the *Instant Structures Mod (ISM)* by Marc Schmidt (alias MaggiCraft).
ISM is a [Forge][forge] mod for the Minecraft Java Edition.
It features a graphical user interface and library of tens of thousands of structures.
Its key functionalities are

* [placing structures][placing],
* [reposition structures][repositioning],
* [removing structures][removing],
* [scanning structures][scanning], and
* [placing geometric shapes][shapes].

Visit the official website [instant-structures-mod.com][website] and check out all ISM features.
It was originally published in the summer of 2014.

## Download

This repository contains a partition of ISM's Java source code.
If you want to use ISM, obtain a copy from the [official website][website].

## Forums

* ISM on [Planetminecraft][pmc]
* ISM on [Minecraft Forum][mcforum]
* Support this project on [Patreon][patreon]
* MaggiCraft's [YouTube channel][youtube]

## Contributing

Please read the [CONTRIBUTING.md](CONTRIBUTING.md) for details of the development process and guidelines we follow within this project.

## Authors

* **Marc Schmidt**, also known as MaggiCraft

## Related Projects

There are several projects with similar functionality to ISM.

* [Instant Massive Structures Mod Unlimited][massive], by [SimJoo][simjoo]
  This mod supports the Minecraft versions from 1.7 to 1.12.
  Like ISM, it features ten thousands of structures and additionally offers moving ones.
  It offers a command-line interface for searching structures but lacks a graphical user interface.
* [WorldEdit][worldedit], by [various developers][devs]
  It supports the Minecraft versions from 1.7 to 1.16.
  WorldEdit lets you place, remove, and scan structures stored as [schematic files][schematic].

[devs]:          https://github.com/EngineHub/WorldEdit/graphs/contributors
[forge]:         https://files.minecraftforge.net/
[massive]:       https://www.planetminecraft.com/mod/11-instant-massive-structures-mod-v10/
[mcforum]:       https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2216378-1-5-update-instant-structures-mod-ism-by
[patreon]:       https://www.patreon.com/MaggiCraft
[placing]:       https://instant-structures-mod.com/tutorials/place-structures/
[pmc]:           https://www.planetminecraft.com/mod/instant-structures-mod-ism/
[removing]:      https://instant-structures-mod.com/tutorials/remove-structures/
[repositioning]: https://instant-structures-mod.com/tutorials/reposition-structures/
[scanning]:      https://instant-structures-mod.com/tutorials/scan-structures/
[schematic]:     https://minecraft.gamepedia.com/Schematic_file_format
[shapes]:        https://instant-structures-mod.com/tutorials/place-shapes/
[simjoo]:        https://www.planetminecraft.com/member/simjoo/
[website]:       https://instant-structures-mod.com/
[worldedit]:     https://github.com/EngineHub/WorldEdit
[youtube]:       https://www.youtube.com/user/MaggiCraft
