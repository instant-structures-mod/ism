# Dependencies

This document gives an overview of the dependencies used in the ISM project.
It answers where and why a dependency is used and how this dependency should be treated with future updates.
Most importantly, it lists the licenses of the dependencies.

## Project-Internal Dependencies

ISM is composed of multiple systems that interact with each other.
These systems are distributed in this repository.

| Dependency     | License | Description                                                                | Action                  |
|----------------|---------|----------------------------------------------------------------------------|-------------------------|
| ism-common     | none    | Interfaces and shared code for the ism-side and minecraft-side.            | add open-source license |
| ism-side       | none    | Business logical and UI independent of the Minecraft.                      | add open-source license |
| minecraft-side | none    | Connection to ism-side<br>Shared functionality for all Minecraft versions. | add open-source license |
| minecraft-stub | none    | Connection to ism-side<br>Minecraft Mock.                                  | add open-source license |
| ism-variants   | none    | Different variants of ISM dependent on their Minecraft and Forge version.  | add open-source license |

## Internal Dependencies

ISM depends on internal dependencies that are not necessarily connected to Minecraft.
These projects are implemented and maintained by Marc Schmidt.

| Dependency  | License | Description                     | Action                               |
|-------------|---------|---------------------------------|--------------------------------------|
| schematic   | none    | reading schematic files         | open-source license for binary       |
| StrRenderer | none    | 3D rendering of structures      | open-source license for binary       |
| manalytics  | none    | tracking with Google Analytics  | open-source license for binary       |
| mcommons    | none    | utility functionality           | open-source license for binary       |
| mcompress   | none    | compression for Java arrays     | open-source license for binary       |
| mgui        | none    | UI framework                    | open-source license for binary       |
| mioutil     | none    | utility for I/O                 | merge to mcommons                    |
| mlog        | none    | logger                          | deprecate and replace by logger      |
| mthread     | none    | utility library for concurrency | replace with Java's standard library |

## Developer Dependencies

Dependencies used during development that are not distributed to binaries.

| Dependency                                 | License               | Description                     | Action                         |
|--------------------------------------------|-----------------------|---------------------------------|--------------------------------|
| mtest                                      | none                  | utility library for testing     | open-source license for binary |
| [Forge][forge]                             | [LPGL 2.1][lpgl21]    | modloader for Minecraft         | keep                           |
| [java-annotations][annotations]            | [Apache 2.0][apache2] | annotations for static analysis | keep                           |
| [JUnit Jupiter API][junit]                 | [EPL 2.0][epl2]       | testing framework               | keep                           |
| [JUnit Jupiter Engine][junit]              | [EPL 2.0][epl2]       | testing framework               | keep                           |
| [JUnit Jupiter Params][junit]              | [EPL 2.0][epl2]       | testing framework               | keep                           |
| [assertj-core][assertjcore]                | [Apache 2.0][apache2] | testing framework               | introduce to other projects    |
| [assertj-assertions-generator][assertjgen] | [Apache 2.0][apache2] | testing framework               | introduce to other projects    |

## Third-Party Dependencies

External binaries and source code that ISM or any internal dependency requires.

| Dependency                      | License               | Description                     |
|---------------------------------|-----------------------|---------------------------------|
| [Adventure NBT package][nbt]    | [MIT][mit]            | various uses                    |

## Licenses

| License               | Dependency Type           | Dependencies |
|-----------------------|---------------------------|--------------|
| [LPGL 2.1][lpgl21]    | development               | +21          |
| none                  | internal                  | 15           |
| [Apache 2.0][apache2] | third party & development | 10           |
| [EPL 2.0][epl2]       | development               | 3            |
| [MIT][mit]            | third party               | 1            |

[apache2]:     https://opensource.org/licenses/Apache-2.0
[epl2]:        https://opensource.org/licenses/EPL-2.0
[lpgl21]:      https://opensource.org/licenses/LGPL-2.1
[mit]:         https://opensource.org/licenses/MIT

[assertjcore]: https://github.com/assertj/assertj-core
[assertjgen]:  https://github.com/assertj/assertj-assertions-generator
[forge]:       https://github.com/MinecraftForge/MinecraftForge
[junit]:       https://junit.org/junit5/

[nbt]:         https://github.com/KyoriPowered/adventure
