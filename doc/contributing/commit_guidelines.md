# Commit Message Guidelines

We structure our commit message by guidelines based on Chris Beam's "[How to Write a Git Commit Message][chrisBeams]" and on [GitLab's guidelines][guidelines].
We do this because consistent commit messages make the commit history more readable.

## Subject Line

The commit subject line...

* ...is written in the imperative mood.
* ...is written as it starts with "If applied, this commit will...".
* ...is limited to 50 characters.
* ...must start with a capital letter.
* ...must not end with a period.
* ...should contain at least three words.
* ...and body must be separated by a blank line.
* ...or body must not contain Emojis.

## Other Guidelines

* Commits that change 30 lines in at least 3 files must describe these changes.
* Describe *what* changed and *why*.
  The *how* can normally be omitted since it is already described by the commit content.
* Use the full URLs of issues and merge request instead of short references, because they are displayed as plain text outside of GitLab.
* Issues and merge requests can be referenced together with `Close:` or `Related:`.
  The former closes the issue when merging to master.
* A merge request should not contain more than 15 commit messages.

## Commit Message Template

```plaintext
# (If applied, this commit will...) <subject> (Max 50 char)
# |<----  Using a Maximum Of 50 Characters  ---->|


# Explain why this change is being made
# |<----   Try To Limit Each Line to a Maximum Of 72 Characters   ---->|

# Provide links or keys to any relevant tickets, articles, or other
# resources. Use the keywords 'Close' and 'Related' together with the
# full URLs of issues and merge requests instead of the short
# references. Because there are displayed as plain text outside of
# GitLab.

# ---- COMMIT END ----
# --------------------
# Remember to
#   use the imperative mood in the subject line
#   capitalize the subject line
#   not end the subject line with a period
#   subject must contain at least 3 words
#   separate subject from the body with a blank line
#   do not use Emojis
#   commits that change 30 or more lines across at least 3 files must
#     describe these changes in the commit body
#   use the body to explain what changed and why
#
# For more information:
#   https://chris.beams.io/posts/git-commit/
#   https://gitlab.com/instant-structures-mod/ism/-/tree/master/doc/development/contributing/commit_message_guidelines.md
# --------------------
```

## Squash Commits

Not all commits are relevant to the history of a branch.
We squash less relevant commits of non-shared branches to keep their history clear.
Candidates for squashing are commits that add documentation, fix typos or simple bugs.

[chrisbeams]: https://chris.beams.io/posts/git-commit/
[guidelines]: https://gitlab.com/gitlab-org/gitlab-foss/-/blob/dbe46935eb1044bd1360776ca684cebcab334003/doc/development/contributing/merge_request_workflow.md#commit-messages-guidelines
