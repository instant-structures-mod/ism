# Issue Workflow

This guideline is based on the [issue workflow][issueworkflow] introduced by the [GitLab project][gitlabproject].
It tailors the presented guidelines to the needs of this project.

## Issue Tracker Guidelines

Search the [issue tracker][issuetracker] for similar entries before submitting a new issue.
You can show your support and emphasize the importance of this issue by joining its discussion or award it with an emoji.

## Issue Triaging

We flag issues with labels for priority and severity.
Therefore we follow the scheme introduced by [GitLab's triage strategy][triage].

### Priority

The priority is used to determine the importance of an issue.
It helps with scheduling it relative to our capacity.

| Priority       | Importance | Intention                                                         |
|----------------|------------|-------------------------------------------------------------------|
| ~"priority::1" | Urgent     | We will address this as soon as possible.                         |
| ~"priority::2" | High       | We will address this soon.                                        |
| ~"priority::3" | Medium     | We want to address this but may have other higher priority items. |
| ~"priority::4" | Low        | We do not know when this will be addressed.                       |

### Severity

The severity labels determine the impact of a bug on users.
Thereby it helps us to determine the urgency of an issue.

| Priority       | Importance | Intention                                                         |
|----------------|------------|-------------------------------------------------------------------|
| ~"priority::1" | Urgent     | We will address this as soon as possible.                         |
| ~"priority::2" | High       | We will address this soon.                                        |
| ~"priority::3" | Medium     | We want to address this but may have other higher priority items. |
| ~"priority::4" | Low        | We do not know when this will be addressed.                       |

## Labels

All labels and their meaning are defined on the [labels page][labelspage].

### Type Labels

Type labels define the kind of issue.
Every issue should have exactly one.
The type labels are:

* ~"type::bug"
* ~"type::docs::feature"
* ~"type::docs::fix"
* ~"type::docs::improvement"
* ~"type::feature::addition"
* ~"type::feature::enhancement"
* ~"type::feature::maintenance"
* ~"type::meta"
* ~"type::tooling::pipelines"
* ~"type::tooling::workflow"

### Variant Labels

We apply variant labels to issues or merge request that target one or more specific Minecraft versions.
Therefore, they are not defined as [scope labels][scopelabels] so that they can be combined.
If an issue or MR belongs to more than three specific Minecraft versions, we apply the ~"variant: multiple" label.

* ~"variant: multiple"
* ~"variant: 1.7"
* ~"variant: 1.8"
* ~"variant: 1.9"
* ~"variant: 1.10"
* ~"variant: 1.11"
* ~"variant: 1.12"
* ~"variant: 1.13"
* ~"variant: 1.14"
* ~"variant: 1.15"
* ~"variant: 1.16"

## Issue Weight

We use issue weights in the exact [same way as the GitLab project][issueweight].

[gitlabproject]: https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/contributing/issue_workflow.md
[issuetracker]:  https://gitlab.com/instant-structures-mod/ism/-/issues
[issueweight]:   https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/contributing/issue_workflow.md#issue-weight
[issueworkflow]: https://gitlab.com/gitlab-org/gitlab-foss/
[labelspage]:    https://gitlab.com/instant-structures-mod/ism/-/labels
[scopelabels]:   https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels
[triage]:        https://about.gitlab.com/handbook/engineering/quality/issue-triage/
