# Contributing to ISM

This guideline defines a regular workflow for participating in the development of the Instant Structures Mod (ISM).
Contributions under the same terms as defined in [LICENSE](../../LICENSE.md) are welcome.

The workflow starts with creating an issue and ends with merging the implementation to a stable branch.
Before doing a particular step of this guideline, ensure that nobody already made it before.
If so, you can use the latest step as a starting point for participation.
You can promote issues and merge request by commenting or reacting with Emojis or by implementing an issue or reviewing a merge request.

## Steps for Realizing a Subject

1. Create an issue for the subject.
   * By doing this adheres to the [issue guidelines](issue_guidelines.md).
   * Assign yourself as the author.
   * Give an estimate of how much time it takes to implement it and track the time while implementing it.
   * Add a matching milestone, if available.
2. If needed, create a branch to implement this issue as described in the [Branches section](#branch-structure)
3. Realize the subject of matter.
   * Adhere to coding conventions.
   * Document the realized functionality together with design decisions.
   * Write automated tests or define manual test cases.
4. Commit regularly while adhering to the [commit message guidelines](commit_guidelines.md)
5. Review the implementation by yourself.
6. Create a merge request.
7. Have your merge request reviewed and receive feedback.
8. Implement or discuss the given feedback.
9. Have your merge request accepted and merged into the required branch.

## Branch Structure

The master branch of this repository is protected.
It contains a stable version of the different ISM variants.<br>
Additionally, there are stable and protected branches for each ISM variant that may diverge from the version in master.
These variant branches follow the naming scheme `variant-X-Y-Z` while `X.Y.Z` is the corresponding Minecraft version (regular expression: `variant(-\d){2,3}`).<br>
Each milestone has its own branch which is normally created from master.
Its name starts with an exclamation mark followed by the milestone number and a short, lowercase variant of the milestone name separated by hyphens (regular expression: `![0-9]+(\-[a-z])?[\-a-z,_]+[a-z]`).
Self-contained realizations of issues may be implemented directly on milestone branches.<br>
If the realization may get more complicated or there is no fitting milestone, create a feature branch for the issue.
These branches use the same naming scheme as the milestone branches but start with a hashtag followed by the shortened issue title they resolve (regular expression: `#[0-9]+(\-[a-z])?[\-a-z,_]+[a-z]`).
Follow-up issues can be implemented on the appropriate branch without creating an issue for them.

## Definition of Done

A contribution is *done* as it meets the following requirements:

1. Clear description explaining the relevancy of the contribution.
2. Working and clean code that is documented when it is needed.
3. Running unit, integration, and system test.
4. Regressions and bugs are covered with tests to reduce the risk of the issue happening again.
5. Changelog entry is added, if necessary.
6. Contribution got reviewed and, if necessary, improved.
7. Merged to a corresponding branch.
