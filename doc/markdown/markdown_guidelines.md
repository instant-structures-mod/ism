# Markdown Guidelines

We write general documentation as markdown files.
Since this repository gets hosted on GitLab, we can and should use the functionalities supported by [GitLab's flavored markdown][gfm].

## Style Guidelines

We adhere to [Ciro Santilli's style guide][styleguide].
It offers [`key:value` pairs][options] to choose from a set of given alternatives.
We use the following configuration of the style guide:

* `wrap:sentence`
* `header:atx`
* `list-marker:asterisk`
* `list-space:1`
* We do not use [empty lines inside lists][emptylines].
* This style guide advises to only use the `1.` marker in ordered lists.
  Furthermore, it recommends only use ordered lists if their items get referenced.
  We deviate from these rules.
  You can use ordered lists to emphasize the order of their items.
  Since markdown documents can be displayed as plain text, we use incremental numbering.
* Write headers in title case according to the [Chicago Manual of Style][chicago].
* Add alternative text to pictures.
* Use a spell checker before you commit your documents.

## Linting

The linter [markdownlint][lint] checks markdown documents for a subset of the specified guidelines.
This tool gets configured with the [markdown_style.rb](../../.markdown-lint.yaml) file.
These rules are documented in the [markdownlint repository][doc].

## Validate Links

The tool [markdown-link-check][mlc] checks if the links of the markdown files are valid and reachable.
It also checks if links to internal files reference an existing one.
Run the following command from the root folder of this repository.

```bash
find . -name \*.md -exec markdown-link-check {} \;
```

## Spell Checking

We use the tool [node-markdown-spellcheck][spellcheck] to spell check markdown documents.
You can run it from the root folder of this dictionary.

```bash
mdspell "**/*.md" -n --en-us
```

Furthermore, you can use the tool [gramma][gramma] to find additional errors.
Take this tool with a grain of salt, since it reports many false-positive errors.
Again, you can run the command from the root directory of this repository.

```bash
find . -name "*.md" -exec sh -c \
  "echo \"check $1\";\
  gramma check -p {} -d typography;\
  echo \" \";\
  read -p \"Press enter to check the next file.\"" _ {} \;
```

[chicago]:    https://en.wikipedia.org/wiki/Title_case#Chicago_Manual_of_Style
[doc]:        https://github.com/DavidAnson/markdownlint/blob/main/doc/Rules.md
[emptylines]: https://cirosantilli.com/markdown-style-guide/#empty-lines-inside-lists
[gfm]:        https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/user/markdown.md
[gramma]:     https://caderek.github.io/gramma/
[lint]:       https://github.com/DavidAnson/markdownlint
[mlc]:        https://github.com/tcort/markdown-link-check
[options]:    https://cirosantilli.com/markdown-style-guide/#options-system
[spellcheck]: https://github.com/lukeapage/node-markdown-spellcheck
[styleguide]: https://cirosantilli.com/markdown-style-guide/
