# Rolling Process

This document describes the steps for releasing a new ISM version.

## Overview

1. Merge changes to master
2. Bump version
  ISM adheres to [semantic versioning][semver].
  Therefore a new release should bump the version accordingly.
  The version has to be updated in the corresponding Gradle build file.
3. Run test successfully
  Ascertain that the current version runs as expected and is of high quality.
  For example, by running all automated tests and execute the non-automated system integration tests.
4. [Update changelog](#update-changelog)
5. Add git tag
* Add the prefix `v`followed by the semantic version number.
6. Publish the binaries of the new version
7. Add entries to the update files
8. Update blog posts
* ISM has posts on [patreon.com][patreon], [planetminecraft][pmc], and [minecraftforum.net][mcforum].
  Update the changelog on these posts and/or comment that a new version was published.
9. Notify people who reported bugs per mail if the bug was fixed.
10. Update the [website][website]
* If major changes were made, check if the content of the website or blog posts has to be updated.

### Update the Changelog

* Update the [changelog file](../../CHANGELOG.md).
  This file contains the changelog for all variants of ISM.
* Add a new header `## MAJOR.MINOR.FIX (YYYY-MM-DD)`
* Link the new release
* Search for [closed issues][closed] since the last release and make sections for:
  1. Added - for new features
  2. Changed - for changes in existing functionality
  3. Deprecated - for once-stable features that get removed in upcoming releases
  4. Removed - for deprecated features the get removed in this release
  5. Fixed - for any bug fixes
  6. Security - for any security issues
* Use the schema `### Section (XY Changes)` where `XY` is the number of entries for this category.
* Each entry has a suffix `(A.B.C, U.V.W-X.Y.Z)` that references the ISM variant the changes are introduced for.
* Each ISM variant has its own changelog file.
  Copy the relevant changes from the overall changelog to it.

GitLab gives excellent examples of how to [write meaningful changelog entries][meaningful].

```markdown
# Changelog

## [3.0.1][3.0.1] (2021-01-15)

### Added
- ... (1.16.4)
- ... (1.15 - 1.16.4)

### Changed
...

[3.0.1]: https://gitlab.com/instant-structures-mod/ism/-/tags/v3.0.1
```

[closed]:     https://gitlab.com/instant-structures-mod/ism/-/issues?scope=all&sort=updated_desc&state=closed&utf8=%E2%9C%93
[meaningful]: https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/changelog.md#writing-good-changelog-entries
[mcforum]:    https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2216378-1-5-update-instant-structures-mod-ism-by?page=30
[patreon]:    https://www.patreon.com/MaggiCraft
[pmc]:        https://www.planetminecraft.com/mod/instant-structures-mod-ism/
[semver]:     http://semver.org/
[website]:    https://instant-structures-mod.com/
