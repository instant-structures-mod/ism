# Coding Conventions

The Java source files of this repository have to follow this style guideline that is based on the [Google Java Style Guide][styleguide].
This style guideline makes a few exceptions from Google's one and imposed further restrictions.

## Derivations and Additions

* Wildcard imports are allowed.
* Intention by three instead of two spaces.
* Continuation indent is six instead of four spaces.
* List annotations never in the same line as a code block.
* Prefixes are commonly used for different code elements.
* `this` to reference member variables should only be used when required.

## Prefixes

We use prefixes for different types of classes and variables.
They clarify the type of the used piece of code.
Additionally, they support code completion by just typing the prefix of a specific type.

Member variables get prefixed with an `m`, parameters with a `p`, and static ones with an `s`.
These prefixes also make the use of `this` in constructors and setters to reference member fields obsolete.

```java
class Foo {

  static int sBar = 42;

  int mFoobar = 0;

  void setFoobar(int pFoobar) {
    mFoobar = pFoobar;
  }
}
```

## Formatting

We format all Java source files in the same way.
Therefore, we provide a [generalized](code_style.editorconfig) and an [Intellij Idea-based](code_style_intellij_idea.xml) configuration file.
The file's content speaks for itself.
You can use one of them to configure your IDE.

## Interfaces

Consider creating an interface for classes that should be instantiated.
The name of the interface must start with an `I` followed by the class name.
It defines the public-accessible functions and decouples them from their specific implementation.
Source code that would use this class should use the interface instead.
Thus, allowing to exchange implementations and keeping the code more maintainable.
In most cases, it is sufficient to document the methods of the interface and define implementation-specifics in the implementing class.

## Enums

The name of an enum must start with an `E` and must be in the singular form.

## Tests

We use the fifth version of the [JUnit testing framework][junit5] to write automated tests.
The tests mirror the package and class structure but reside in another parent folder.
Abstract test classes that define a test harness get suffixed by `TestCase`.
Test cases should be implemented in a class file named as the original one and suffixed with `Test`.
These class files should reside in the mirrored package of the original one.

## Annotations

We use [JetBrains Annotations][annotations] to infer additional information about fields and methods.
The most important annotations are `@Nullable` and `@NotNull` that check nullability for variables, parameters, and return values.
You should annotate these annotations generously since they are an excellent tool for reducing failures from `NullPointerExceptions`.

[annotations]:          https://github.com/JetBrains/java-annotations
[junit5]:               https://junit.org/junit5/
[googleJavaStyleGuide]: https://google.github.io/styleguide/javaguide.html
