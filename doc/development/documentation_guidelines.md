# Documentation Guidelines

We adhere to the guidelines imposed by the article "[How to Write Doc Comments for the Javadoc Tool][doc]" published by Oracle.

[doc]: https://www.oracle.com/technical-resources/articles/java/javadoc-tool.html
