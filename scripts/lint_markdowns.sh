#!/bin/bash
# Executes spellchecker for markdown files.

# check spelling and grammar
echo "Spell Check"
mdspell "**/*.md" -n --en-us

find . -name "*.md" -exec sh -c \
  'echo "check" $1
  gramma check -p $1 -d typography;
  echo " "
  read -p "Press enter to check the next file."' _ {} \;
