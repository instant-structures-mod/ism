#!/bin/bash
# Runs a docker container which generates a domain-specific test language.
#
# $1: bundled jar file that contains all classes for which test classes get generated
# $2: output folder for the generated classes
# $3: package structure for which the classes get generated

mkdir -p "$2"
docker run \
  --mount type=bind,source="$1",target=/input/input.jar \
  --mount type=bind,source="$2",target=/output/ \
  -e package="$3" \
  marcschmidt98/assertj-generator:initial
