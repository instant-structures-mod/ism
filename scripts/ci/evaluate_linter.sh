#!/bin/bash
# Evaluates if a linter found any errors and exits correspondingly.
# Prints the output of the linter.
# $1 identifier of the linter as defined by mega-linter

ERROR="report/linters_logs/ERROR-$1.log"
SUCCESS="report/linters_logs/SUCCESS-$1.log"

if test -f "${ERROR}"; then
  cat "${ERROR}"
  exit 1
fi

if test -f "${SUCCESS}"; then
  cat "${SUCCESS}"
  exit 0
fi
