#!/bin/bash
# Determines the status of the Gradle task 'dependencyUpdates' of the com.github.ben-manes.versions-
# plugin. Therefore, it reads a created plain-text report file.
#
# $1: path to the report file

if grep -q "later milestone versions" "$1"; then
  echo "Uses dependencies with later milestone versions."
  exit 1
fi

if grep -q "Failed to determine" "$1"; then
  echo "Failed to resolve dependencies."
  exit 1
fi

echo "All dependencies are up-to-date."
exit 0
