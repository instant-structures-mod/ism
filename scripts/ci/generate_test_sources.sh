#!/bin/bash
# Generates test classes for a domain specific test language. It gets executed from inside Docker
# while the CI pipeline run.
#
# $1: bundled jar file that contains all classes for which test classes get generated
# $2: output folder for the generated classes
# $3: package structure for which the classes get generated

cp "$1" /home/lib/
cd /home/ || exit
bash generate-assertions.sh "$3"

mkdir -p "$2"
cp -R "$3" "$2"
