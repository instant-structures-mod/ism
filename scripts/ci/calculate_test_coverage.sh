#!/bin/bash
# Calculates the test coverage for all sub-projects.
#
# $1: path to the report file in the CSV format

awk -F"," '{ all += $4 + $5; covered += $5 } END { print 100 * covered / all, "% covered" }' "$1"
