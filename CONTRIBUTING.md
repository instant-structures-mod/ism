# Contributing

We welcome contributions from the community.
Contributions are considered under the same terms as stated in [LICENSE](LICENSE.md).
View the documentation of the [contribution workflow](doc/contributing/index.md) to get started.
We have several documents describing parts of this process.

## Contribute to ISM

[General contribution process](doc/contributing/index.md)

### Realize a Subject

[How to implement an issue](doc/contributing/index.md#steps-for-realizing-a-subject)

### Branch Structure

[How the branches of this repository are structured](doc/contributing/index.md#branch-structure)

### Definition of Done

[When an issue got completed](doc/contributing/index.md#definition-of-done)

## Commit Guidelines

[How we write commit messages](doc/contributing/commit_guidelines.md)

## Issue Guidelines

[How we create GitLab issues](doc/contributing/issue_guidelines.md)

### Priority Labels

[How to prioritize issues](doc/contributing/issue_guidelines.md#priority)

### Severity Labels

[How to define severity of issues](doc/contributing/issue_guidelines.md#severity)

### Type Labels

[How to define the type of issues](doc/contributing/issue_guidelines.md#type-labels)

### Variant Labels

[How to assign the ISM variant to an issue](doc/contributing/issue_guidelines.md#variant-labels)

## Coding Conventions

[Guidelines for implementing Java code](doc/development/coding_conventions.md)

### Formatting Guidelines

[How Java code get formatted](doc/development/coding_conventions.md#formatting)

## Documentation Guidelines

[Hot to write documentation](doc/development/documentation_guidelines.md)

## Markdown Guidelines

[Guidelines for writing markdown documents](doc/markdown/markdown_guidelines.md)

## Release Process

[What steps to take for releasing an ISM version](doc/development/release_process.md)

### Update Changelog

[How to update the changelog](doc/development/release_process.md#update-changelog)
