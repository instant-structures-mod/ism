# Credits

2014-present, Instant Structures Mod (ISM) by Marc Schmidt (alias MaggiCraft)

The Huffman compression algorithm builds on the work by Nayuki [1].
It is in the de.ism.database.huffman-packackage and is licensed under MIT License [2].

[1]: https://github.com/nayuki/Reference-Huffman-coding
[2]: https://opensource.org/licenses/MIT
